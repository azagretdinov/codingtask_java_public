package org.bitbucket.azagretdinov.geekforgeek.minimumdistinctids;


import org.bitbucket.azagretdinov.geekforgeek.uglynumber.GFGUglyNumber;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class GFGMinimumDistinctIdsTest {
    
    @Rule
    public SystemOutRule out = new SystemOutRule()
                                   .enableLog()
                                   .muteForSuccessfulTests();
    
    @Rule
    public TextFromStandardInputStream in = TextFromStandardInputStream.emptyStandardInputStream();
    
    @Test
    public void should_return_the_size_of_array_minus_one_when_m_is_1_and_all_values_are_distinct() throws IOException {
    
        in.provideLines(
            "1",
            "4",
            "1 2 3 4",
            "1"
        );
    
        GFGMinimumDistinctIds.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
    
        assertThat(log)
            .isEqualTo("3\n");
        
    }
    
    @Test
    public void should_return_one_if_all_elements_the_same_and_m_less_n() throws IOException {
        in.provideLines(
            "1",
            "4",
            "3 3 3 3",
            "2"
        );
    
        GFGMinimumDistinctIds.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
    
        assertThat(log)
            .isEqualTo("1\n");
    }
    
    @Test
    public void should_return_remove_element_wich_repeat_less_count_time() throws IOException {
        in.provideLines(
            "2",
            "7",
            "1 3 3 2 4 2 6",
            "2",
            "8",
            "2 4 1 5 3 5 1 3",
            "2"
        );
    
        GFGMinimumDistinctIds.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
    
        assertThat(log)
            .isEqualTo("3\n3\n");
    }
    
    @Test
    public void should_return_0_for_m_equals_to_n() throws IOException {
    
        in.provideLines(
            "1",
            "4",
            "1 2 3 4",
            "4"
        );
    
        GFGMinimumDistinctIds.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
    
        assertThat(log)
            .isEqualTo("0\n");
        
    }
}