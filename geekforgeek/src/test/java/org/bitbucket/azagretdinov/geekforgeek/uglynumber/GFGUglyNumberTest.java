package org.bitbucket.azagretdinov.geekforgeek.uglynumber;


import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class GFGUglyNumberTest {
    
    @Rule
    public SystemOutRule out = new SystemOutRule()
                                       .enableLog()
                                       .muteForSuccessfulTests();
    
    @Rule
    public TextFromStandardInputStream in = TextFromStandardInputStream.emptyStandardInputStream();
    
    
    @Test
    public void should_return_1_for_1() {
        
        in.provideLines("1", "1");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("1\n");
    }
    
    @Test
    public void should_return_2_for_2() {
        
        in.provideLines("1", "2");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("2\n");
    }
    
    @Test
    public void should_return_3_for_3() {
        
        in.provideLines("1", "3");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("3\n");
    }
    
    @Test
    public void should_return_5_for_5() {
        
        in.provideLines("1", "5");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("5\n");
    }
    
    @Test
    public void should_return_result_for_multiply_test() {
        in.provideLines("2", "10", "4");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("12\n4\n");
    }
    
    @Test
    public void should_return_result_for_multiply_test_max_in_the_end() {
        in.provideLines("2", "4", "10");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("4\n12\n");
    }
    
    @Test
    public void should_return_result_for_max_n() {
        in.provideLines("1", "500");
        
        GFGUglyNumber.main(new String[0]);
    
        final String log = out.getLogWithNormalizedLineSeparator();
        
        assertThat(log)
            .isEqualTo("937500\n");
    }
}