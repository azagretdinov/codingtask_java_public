/**
 * Given an array of items, an i-th index element denotes the item id’s and given a number m, the task is to remove m elements such that there should be minimum distinct id’s left.Print the number of distinct id’s.
 * <p>
 * Input:
 * The first line of the input contains a single integer T, denoting the number of test cases. Then T test case follows, the three lines of the input, the first line contains N, denoting number of elements in an array,second line contains N elements/ids, and third line contains the number M.
 * <p>
 * Output:
 * For each test case, print the minimum number of distinct ids.
 * <p>
 * Constraints:
 * 1<=T<=100
 * 1<=N<=100
 * 1<=arr[i]<=10^6
 * 1<=M<=100
 * <p>
 * Example:
 * Input:
 * 2
 * 6
 * 2 2 1 3 3 3
 * 3
 * 8
 * 2 4 1 5 3 5 1 3
 * 2
 * Output:
 * 1
 * 3
 */
package org.bitbucket.azagretdinov.geekforgeek.minimumdistinctids;