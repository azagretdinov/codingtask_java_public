package org.bitbucket.azagretdinov.geekforgeek.minimumdistinctids;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class GFGMinimumDistinctIds {
    public static void main(String[] args) throws IOException {
        
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
        int tc = Integer.parseInt(reader.readLine());
        
        while (--tc >= 0) {
            final int n = Integer.parseInt(reader.readLine());
            final int[] a = readArray(reader, n);
            final int m = Integer.parseInt(reader.readLine());
            
            System.out.println(removeIds(a, n, m));
        }
        
    }
    
    private static int removeIds(final int[] a, final int n, final int m) {
        Map<Integer, Integer> ids = new HashMap<>();
        
        for (int v : a) {
            Integer count = ids.get(v);
            if (count == null) {
                count = 0;
            }
            ids.put(v, ++count);
        }
        
        
        final List<Entry<Integer, Integer>> entries = new ArrayList<>(ids.entrySet());
        entries.sort(Comparator.comparing(Entry::getValue));
        final Iterator<Entry<Integer, Integer>> itr = entries.iterator();
        
        int i = m;
        
        while (i > 0 && itr.hasNext()) {
            final Entry<Integer, Integer> entry = itr.next();
            if (i >= entry.getValue()) {
                itr.remove();
            }
            i -= entry.getValue();
        }
        
        return entries.size();
    }
    
    private static int[] readArray(final BufferedReader reader, final int n) throws IOException {
        final int[] r = new int[n];
        
        final String[] values = reader.readLine().split(" ");
        
        for (int i = 0; i < n; i++) {
            r[i] = Integer.parseInt(values[i]);
        }
        
        return r;
    }
}
