package org.bitbucket.azagretdinov.geekforgeek.uglynumber;

import java.util.Scanner;

public class GFGUglyNumber {
    public static void main(String[] args) {
        
        final Scanner scanner = new Scanner(System.in);
        final int tc = scanner.nextInt();
        
        int[] uglyNumber = new int[501];
        
        uglyNumber[0] = 1;
        
        int[] indexMax = new int[3];
        int[] next = new int[3];
        next[0] = 2;
        next[1] = 3;
        next[2] = 5;
        
        int currentMax = 0;
        
        for (int i = 0; i < tc; i++) {
            final int n = scanner.nextInt();
            if (currentMax < n) {
                fillUglyNumbers(currentMax, indexMax, next, uglyNumber, n);
                currentMax = n;
            }
            
            System.out.println(uglyNumber[n - 1]);
        }
        
    }
    
    private static void fillUglyNumbers(final int currentMax, final int[] indexes, final int[] next, final int[] uglyNumber, final int n) {
        int nextUglyNumber;
        
        for (int i = currentMax + 1; i <= n; i++) {
            nextUglyNumber = Math.min(
                next[0],
                Math.min(
                    next[1],
                    next[2]
                )
            );
            uglyNumber[i] = nextUglyNumber;
            
            if (nextUglyNumber == next[0]) {
                ++indexes[0];
                next[0] = uglyNumber[indexes[0]] * 2;
            }
            if (nextUglyNumber == next[1]) {
                ++indexes[1];
                next[1] = uglyNumber[indexes[1]] * 3;
            }
            if (nextUglyNumber == next[2]) {
                ++indexes[2];
                next[2] = uglyNumber[indexes[2]] * 5;
            }
        }
    }
}
