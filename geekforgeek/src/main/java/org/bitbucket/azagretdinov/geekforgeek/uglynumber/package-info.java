/**
 * Ugly Numbers
 * Show Topic Tags           Goldman-Sachs
 * Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, … shows the first 11 ugly numbers. By convention, 1 is included. Write a program to find Nth Ugly Number.
 * <p>
 * Input:
 * <p>
 * The first line of input contains an integer T denoting the number of test cases.
 * The first line of each test case is N.
 * <p>
 * Output:
 * <p>
 * Print the Nth Ugly Number.
 * <p>
 * Constraints:
 * <p>
 * 1 ≤ T ≤ 100
 * 1 ≤ N ≤ 500
 * <p>
 * Example:
 * <p>
 * Input:
 * 2
 * 10
 * 4
 * <p>
 * Output:
 * 12
 * 4
 */
package org.bitbucket.azagretdinov.geekforgeek.uglynumber;