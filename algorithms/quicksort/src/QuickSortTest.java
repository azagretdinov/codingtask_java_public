import org.junit.Test;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class QuickSortTest {

    private Random random = new Random();

    @Test
    public void shouldSortEmptyArray() {
        final int[] array = getArray();
        sort(array);
        assertThat(array).isEmpty();
    }

    @Test
    public void shouldSortOneElementArray() {
        final int[] array = getArray(10);
        sort(array);
        assertThatArraySorted(array, 10);
    }

    @Test
    public void shouldShortTwoElementArray() {
        assertThatArraySorted(sort(getArray(11, 10)), 10, 11);
        assertThatArraySorted(sort(getArray(10, 11)), 10, 11);
        assertThatArraySorted(sort(getArray(11, 11)), 11, 11);
    }

    @Test
    public void shouldSortThreeElementArray() {
        assertThatArraySorted(sort(getArray(12, 10, 11)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(11, 10, 12)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(10, 11, 12)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(12, 11, 10)), 10, 11, 12);
    }

    @Test
    public void shouldSortFourElementArray() {
        assertThatArraySorted(sort(getArray(12, 10, 14, 11)), 10, 11, 12, 14);
    }

    private int[] getArray(int... in) {
        return in;
    }

    private void assertThatArraySorted(int[] a, int... elements) {
        assertThat(a).hasSameSizeAs(elements);
        assertThat(a).containsExactly(elements);
    }

    private int[] sort(int[] a) {
        final int length = a.length;
        if (length < 2) {
            return a;
        }
        sort(a, 0, length - 1);
        return a;
    }

    private void sort(int[] a, int start, int end) {
        if (end - start < 1) {
            return;
        }

        final int pivot = getPivot(a, start, end);
        int pivotIndex = partition(a, start, end, pivot);

        sort(a, start, pivotIndex - 1);
        sort(a, pivotIndex + 1, end);
    }

    private int getPivot(int[] a, int start, int end) {
        int pivotIndex = start + random.nextInt(end - start + 1);
        int pivot = a[pivotIndex];
        swap(a, pivotIndex, end);
        return pivot;
    }

    private int partition(int[] a, int start, int end, int pivot) {
        int left = start - 1;
        int right = start;

        while (right < end) {
            if (a[right] <= pivot) {
                left++;
                swap(a, left, right);
            }
            right++;
        }

        swap(a, ++left, right);
        return left;
    }

    private void swap(int[] a, int from, int to) {
        if (from == to) {
            return;
        }
        int t = a[from];
        a[from] = a[to];
        a[to] = t;
    }

}
