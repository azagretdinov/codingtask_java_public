package org.bitbucket.azagretdinov.quicksort;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QuickSortTest {
    
    @Test
    public void shouldSortEmptyArray() {
        final int[] array = getArray();
        sort(array);
        assertThat(array).isEmpty();
    }
    
    @Test
    public void shouldSortOneElementArray() {
        final int[] array = getArray(10);
        sort(array);
        assertThatArraySorted(array, 10);
    }
    
    @Test
    public void shouldShortTwoElementArray() {
        assertThatArraySorted(sort(getArray(11, 10)), 10, 11);
        assertThatArraySorted(sort(getArray(10, 11)), 10, 11);
        assertThatArraySorted(sort(getArray(11, 11)), 11, 11);
    }
    
    @Test
    public void shouldSortThreeElementArray() {
        assertThatArraySorted(sort(getArray(12, 10, 11)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(11, 10, 12)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(10, 11, 12)), 10, 11, 12);
        assertThatArraySorted(sort(getArray(12, 11, 10)), 10, 11, 12);
    }
    
    @Test
    public void shouldSortFourElementArray() {
        assertThatArraySorted(sort(getArray(12, 10, 14, 11)), 10, 11, 12, 14);
    }
    
    private int[] getArray(int... in) {
        return in;
    }
    
    private void assertThatArraySorted(int[] a, int... elements) {
        assertThat(a).hasSameSizeAs(elements);
        assertThat(a).containsExactly(elements);
    }
    
    private int[] sort(int[] array) {
        return new Solution().sort(array);
    }
    
}
