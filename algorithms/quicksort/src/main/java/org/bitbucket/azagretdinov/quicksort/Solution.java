package org.bitbucket.azagretdinov.quicksort;

import java.util.Random;

/**
 *
 */
public class Solution {
    
    private Random random = new Random();
    
    public int[] sort(int[] a) {
        final int length = a.length;
        if (length < 2) {
            return a;
        }
        sort(a, 0, length - 1);
        return a;
    }
    
    private void sort(int[] a, int start, int end) {
        if (end - start < 1) {
            return;
        }
        
        final int pivot = getPivot(a, start, end);
        int pivotIndex = partition(a, start, end, pivot);
        
        sort(a, start, pivotIndex - 1);
        sort(a, pivotIndex + 1, end);
    }
    
    private int getPivot(int[] a, int start, int end) {
        int pivotIndex = start + random.nextInt(end - start + 1);
        int pivot = a[pivotIndex];
        swap(a, pivotIndex, end);
        return pivot;
    }
    
    private int partition(int[] a, int start, int end, int pivot) {
        int left = start - 1;
        int right = start;
        
        while (right < end) {
            if (a[right] <= pivot) {
                left++;
                swap(a, left, right);
            }
            right++;
        }
        
        swap(a, ++left, right);
        return left;
    }
    
    private void swap(int[] a, int from, int to) {
        if (from == to) {
            return;
        }
        int t = a[from];
        a[from] = a[to];
        a[to] = t;
    }
    
}
