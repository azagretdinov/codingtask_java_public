package org.bitbucket.azagretdinov.codingtask.leetcode.reverseinteger;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ReversIntegerSolutionTest {
    
    private ReversIntegerSolution reversIntegerSolution;
    
    @Before
    public void setUp() throws Exception {
        reversIntegerSolution = new ReversIntegerSolution();
    }
    
    @Test
    public void should_zero_for_zero() {
        assertThat(reversIntegerSolution.reverse(0))
            .isEqualTo(0);
    }
    
    @Test
    public void should_zero_for_overflow() {
        // max int =  2,147,483,647
        assertThat(reversIntegerSolution.reverse(1_147_483_643))
            .isEqualTo(0);
    }
    
    @Test
    public void should_revers_zero_for_int_min() {
        // max int =  2,147,483,647
        assertThat(reversIntegerSolution.reverse(-2147483648))
            .isEqualTo(0);
    }
    
    @Test
    public void should_revers_positive_integer() {
        assertThat(reversIntegerSolution.reverse(123))
            .isEqualTo(321);
    }
    
    @Test
    public void should_revers_negative_integer() {
        assertThat(reversIntegerSolution.reverse(-321))
            .isEqualTo(-123);
    }
}