package org.bitbucket.azagretdinov.codingtask.leetcode.macsubarray;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MaxSubarraySolutionTest {
    
    private MaxSubarraySolution maxSubarraySolution;
    
    @Before
    public void setUp() throws Exception {
        maxSubarraySolution = new MaxSubarraySolution();
    }
    
    @Test
    public void should_return_element_if_array_one_element() {
        assertThat(maxSubarraySolution.maxSubArray(1))
            .isEqualTo(1);
    }
    
    @Test
    public void should_return_maximum_of_two_values_for_two_element_array() {
        assertThat(maxSubarraySolution.maxSubArray(1,2, -1))
            .as("3 is ")
            .isEqualTo(3);
        
    }
    
}