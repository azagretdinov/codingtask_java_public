package org.bitbucket.azagretdinov.codingtask.leetcode.groupanagrams;


import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class GroupAnagramsSolutionTest {
    
    private GroupAnagramsSolution objectUnderTest;
    
    @Before
    public void setUp() throws Exception {
        objectUnderTest = new GroupAnagramsSolution();
    }
    
    @Test
    public void should_return_empty_list_for_null_array() {
        assertThat(objectUnderTest.groupAnagrams(null))
            .isEmpty();
    }
    
    @Test
    public void should_return_empty_list_for_empty_array() {
        assertThat(objectUnderTest.groupAnagrams(new String[0]))
            .isEmpty();
    }
    
    @Test
    public void should_return_one_anagram_for_one_element() {
        
        final List<List<String>> anagrams = objectUnderTest.groupAnagrams(new String[]{"eat"});
        assertThat(anagrams)
            .hasSize(1);
        
        assertThat(anagrams.get(0))
            .contains("eat");
        
    }
    
    @Test
    public void should_return_two_anagram_for_two_different_element() {
        
        final String[] data = {"eat", "bat"};
        final List<List<String>> anagrams = objectUnderTest.groupAnagrams(data);
        assertThat(anagrams)
            .hasSize(2);
    
        assertThat(anagrams.get(0))
            .contains("bat");
        
        assertThat(anagrams.get(1))
            .contains("eat");
        
    }
    
    @Test
    public void should_return_one_list_of_anagrams_for_two_anagrams() {
        
        final String[] data = {"eat", "tea"};
        final List<List<String>> anagrams = objectUnderTest.groupAnagrams(data);
        assertThat(anagrams)
            .hasSize(1);
        
        assertThat(anagrams.get(0))
            .containsExactlyInAnyOrder("eat", "tea");
    }
    
    @Test
    public void should_return_two_list_of_anagrams_for_two_groups_anagrams() {
        
        final String[] data = {"eat", "tea", "door", "rood"};
        final List<List<String>> anagrams = objectUnderTest.groupAnagrams(data);
        assertThat(anagrams)
            .hasSize(2);
        
        assertThat(anagrams.get(0))
            .containsExactlyInAnyOrder("eat", "tea");
        
        assertThat(anagrams.get(1))
            .containsExactlyInAnyOrder("door", "rood");
    }
    
    @Test
    public void should_return_three_groups() {
        
        final String[] data = {"eat", "tea", "tan", "ate", "nat", "bat"};
        final List<List<String>> anagrams = objectUnderTest.groupAnagrams(data);
        assertThat(anagrams)
            .hasSize(3);
        
        assertThat(anagrams.get(1))
            .containsExactlyInAnyOrder("ate", "eat","tea");
        
        assertThat(anagrams.get(2))
            .containsExactlyInAnyOrder("nat","tan");
        
        assertThat(anagrams.get(0))
            .containsExactlyInAnyOrder("bat");
    }
    
    @Test
    public void should_pass_performance_test() throws IOException, URISyntaxException {
        final URI uri = ClassLoader.getSystemResource("group_anagrams_performance").toURI();
        final List<String> strings = Files.readAllLines(Paths.get(uri));
    
        final String[] array = strings.toArray(new String[strings.size()]);
    
        final long start = System.nanoTime();
    
        objectUnderTest.groupAnagrams(array);
    
        final long end = System.nanoTime();
        
        System.out.print(TimeUnit.NANOSECONDS.toMillis(end - start) + "ms");
    
    }
    
}