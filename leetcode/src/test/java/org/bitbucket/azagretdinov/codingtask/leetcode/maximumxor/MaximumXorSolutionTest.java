package org.bitbucket.azagretdinov.codingtask.leetcode.maximumxor;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MaximumXorSolutionTest {
    
    private MaximumXorSolution maximumXorSolution;
    
    @Before
    public void setUp() throws Exception {
        maximumXorSolution = new MaximumXorSolution();
    }
    
    @Test
    public void should_return_xor_two_numbers() {
        final int val = maximumXorSolution.findMaximumXOR(new int[]{10, 23});
        
        assertThat(val)
            .isEqualTo(10 ^ 23);
    }
    
    @Test
    public void should_return_max_xor_numbers() {
        final int val = maximumXorSolution.findMaximumXOR(new int[]{3, 10, 5, 25, 2, 8});
        
        assertThat(val)
            .isEqualTo(5 ^ 25);
    }
    
}