package org.bitbucket.azagretdinov.codingtask.leetcode.permutationinstring;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PermutationSolutionTest {
    
    private PermutationSolution permutationSolution;
    
    @Before
    public void setUp() throws Exception {
        permutationSolution = new PermutationSolution();
    }
    
    @Test
    public void should_return_true_for_one_char_if_char_exist_in_string() {
        assertThat(permutationSolution.checkInclusion("a", "bdfgade")).isTrue();
    }
    
    @Test
    public void should_return_false_for_one_char_if_char_not_exist_in_string() {
        assertThat(permutationSolution.checkInclusion("a", "bdfgde")).isFalse();
    }
    
    @Test
    public void should_return_true_for_two_char_if_chars_exist_in_string_in_same_order() {
        assertThat(permutationSolution.checkInclusion("ad", "bdfgade"))
            .withFailMessage("ad -> bdfgade")
            .isTrue();
    }
    
    @Test
    public void should_return_true_for_two_char_if_chars_not_exist_in_string_in_same_order() {
        assertThat(permutationSolution.checkInclusion("ad", "bdfgagde")).isFalse();
    }
    
    
    @Test
    public void should_return_true_for_two_char_if_chars_exist_in_string_not_in_same_order() {
        assertThat(permutationSolution.checkInclusion("ad", "bdfgade"))
            .withFailMessage("da -> bdfgade")
            .isTrue();
    }
    
    @Test
    public void should_return_true_for_repeatable_chars_if_chars_exist_in_string_in_any_order() {
        assertThat(permutationSolution.checkInclusion("gff", "bdfgfade"))
            .withFailMessage("gff -> bdfgfade")
            .isTrue();
        assertThat(permutationSolution.checkInclusion("adage", "asdgeaa"))
            .withFailMessage("adage -> asdgeaa")
            .isTrue();
        assertThat(permutationSolution.checkInclusion("adc", "dcda"))
            .withFailMessage("adc -> dcda")
            .isTrue();
    }
    
    @Test
    public void should_return_false_for_repeatable_chars_if_chars_exist_only_one_in_s1() {
        assertThat(permutationSolution.checkInclusion("gaf", "bdgffade"))
            .withFailMessage("gaf -> bdgffade")
            .isFalse();
    }
}