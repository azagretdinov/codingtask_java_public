package org.bitbucket.azagretdinov.codingtask.leetcode.validparentheses;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidParenthesesSolutionTest {
    
    private ValidParenthesesSolution objectUnderTest;
    
    @Before
    public void setUp() throws Exception {
        objectUnderTest = new ValidParenthesesSolution();
    }
    
    @Test
    public void should_return_true_for_all_closed() {
        assertThat(objectUnderTest.isValid("()")).isTrue();
        assertThat(objectUnderTest.isValid("{}")).isTrue();
        assertThat(objectUnderTest.isValid("[]")).isTrue();
        assertThat(objectUnderTest.isValid("{[]}()")).isTrue();
    }
    
    @Test
    public void should_return_false_if_sam_no_closed() {
        assertThat(objectUnderTest.isValid("(")).isFalse();
        assertThat(objectUnderTest.isValid("()}")).isFalse();
        assertThat(objectUnderTest.isValid("([)]")).isFalse();
        assertThat(objectUnderTest.isValid("{{)}")).withFailMessage("{{)}").isFalse();
    }
    
}