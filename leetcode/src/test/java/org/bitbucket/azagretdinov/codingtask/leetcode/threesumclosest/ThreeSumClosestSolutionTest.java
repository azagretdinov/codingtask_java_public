package org.bitbucket.azagretdinov.codingtask.leetcode.threesumclosest;


import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ThreeSumClosestSolutionTest {
    
    private ThreeSumClosestSolution threeSumClosestSolution;
    
    @Before
    public void setUp() throws Exception {
        threeSumClosestSolution = new ThreeSumClosestSolution();
    }
    
    @Test
    public void should_return_sum_of_three_for_3_size_array() {
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(1, 2, 3), 1)).isEqualTo(6);
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(0, 0, 0), 1)).isEqualTo(0);
    }
    
    @Test
    public void should_return_sum_of_three_for_4_size_array() {
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(1, 2, 3, -1), 1)).isEqualTo(2);
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(1, 2, 3, 4), 1)).isEqualTo(6);
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(-4, -1, 2, 1), 1)).isEqualTo(2);
        assertThat(threeSumClosestSolution.threeSumClosest(asArray(0,2,1,-3), 1)).isEqualTo(0);
    }
    
    @Test
    public void should_return_sum_of_three_for_max() {
        assertThat(
            threeSumClosestSolution.threeSumClosest(
                asArray(-1, 2, 8,10, 20, 30, -10),
                100)
        ).isEqualTo(60);
    }
    
    private int[] asArray(final int... e) {
        return e;
    }
    
}