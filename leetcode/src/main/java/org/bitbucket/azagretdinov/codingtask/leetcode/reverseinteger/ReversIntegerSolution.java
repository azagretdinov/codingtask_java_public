package org.bitbucket.azagretdinov.codingtask.leetcode.reverseinteger;


public class ReversIntegerSolution {
    public int reverse(int x) {
        final int sign = x < 0 ? -1 : 1;
        
        long result = 0;
        
        long v = Math.abs((long) x);
        final int minInt = sign == -1 ? -1 : 0;
        
        while (v != 0) {
            final long a = v % 10;
            result = result * 10 + a;
            if (Integer.MAX_VALUE + minInt < result) {
                return 0;
            }
            v /= 10;
        }
        
        return (int) (result * sign);
    }
}
