package org.bitbucket.azagretdinov.codingtask.leetcode.threesumclosest;

import java.util.Arrays;

public class ThreeSumClosestSolution {
    public int threeSumClosest(int[] nums, int target) {
        if (nums.length == 3) {
            return nums[0] + nums[1] + nums[2];
        }
        
        Arrays.sort(nums);
        
        final int n = nums.length;
        int r = nums[0] + nums[1] + nums[n - 1];
        int diff = Math.abs(r - target);
        
        for(int i = 0; i < n - 2; i ++){
            int start = i + 1;
            int end = n - 1;
            while(start < end){
                final int sum = nums[i] + nums[start] + nums[end];
                if(sum > target){
                    --end;
                    
                }else {
                    ++start;
                }
                if (Math.abs(sum - target) < diff) {
                    r = sum;
                    diff = Math.abs(r - target);
                }
            }
        }
        
        return r;
    }
}

