package org.bitbucket.azagretdinov.codingtask.leetcode.permutationinstring;

public class PermutationSolution {
    public boolean checkInclusion(String s1, String s2) {
        
        final int n = s1.length();
        final int m = s2.length();
        
        if (n > m) {
            return false;
        }
        
        int[] count = new int[26];
        for (int i = 0; i < n; i++) {
            count[s1.charAt(i) - 'a']++;
            count[s2.charAt(i) - 'a']--;
        }
        
        if (isPermutation(count)) {
            return true;
        }
        
        for (int i = n; i < m; i++) {
            count[s2.charAt(i) - 'a']--;
            count[s2.charAt(i - n) - 'a']++;
            if (isPermutation(count)){
                return true;
            }
        }
        
        return false;
    }
    
    private boolean isPermutation(final int[] count) {
        for (int i : count) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
    
    
}
