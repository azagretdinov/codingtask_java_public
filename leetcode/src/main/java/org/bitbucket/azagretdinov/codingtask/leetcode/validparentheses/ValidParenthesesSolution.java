package org.bitbucket.azagretdinov.codingtask.leetcode.validparentheses;

import java.util.Arrays;
import java.util.Stack;

public class ValidParenthesesSolution {
    
    private static final char[] OPEN = {'(', '[', '{'};
    private static final char[] CLOSE = {')', ']', '}'};
    
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        
        for (char c : s.toCharArray()) {
            if (isOpen(c)) {
                stack.push(c);
            } else if (isClose(c)) {
                if (stack.isEmpty()) {
                    return false;
                }
                char open = stack.pop();
                if (!sameType(open, c)) {
                    return false;
                }
            }
        }
        
        return stack.isEmpty();
    }
    
    private boolean sameType(final char open, final char close) {
        return Math.abs(close - open) <= 2;
    }
    
    private boolean isClose(final char c) {
        return Arrays.binarySearch(CLOSE, c) >= 0;
    }
    
    private boolean isOpen(final char c) {
        return Arrays.binarySearch(OPEN, c) >= 0;
    }
}
