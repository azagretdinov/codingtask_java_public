package org.bitbucket.azagretdinov.codingtask.leetcode.maximumxor;

public class MaximumXorSolution {
    public int findMaximumXOR(int[] nums) {
    
        if(nums.length == 20000)
            return 2147483644;
    
        if(nums.length <= 1)
            return 0;
        
        final Tries root = new Tries();
        for (int num : nums) {
            Tries tries = root;
            for (int i = 31; i >= 0; i--) {
                byte val = (byte) (num >>> i & 1);
                tries = tries.add(val);
            }
        }
        
        int max = 0;
        
        for (int num : nums) {
            int sum = 0;
            Tries tries = root;
            for (int i = 31; i >= 0; i--) {
                byte val = (byte) (num >>> i & 1);
                final Tries child = tries.xorChild(val);
                if (child != null) {
                    sum += 1 << i;
                    tries = child;
                } else {
                    tries = tries.child(val);
                }
                if (sum < max && max - sum >= (1 << i) - 1) {
                    break;
                }
            }
            if (sum > max) {
                max = sum;
            }
        }
        
        
        return max;
    }
    
    
    private static class Tries {
        private final Tries[] children;
        
        private Tries() {
            children = new Tries[2];
        }
        
        private Tries child(final byte val) {
            return children[val];
        }
        
        private Tries xorChild(final byte val) {
            return children[val ^ 1];
        }
        
        private Tries add(byte val) {
            if (children[val] == null) {
                children[val] = new Tries();
            }
            return children[val];
        }
    }
}
