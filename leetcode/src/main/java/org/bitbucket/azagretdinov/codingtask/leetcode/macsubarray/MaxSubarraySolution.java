package org.bitbucket.azagretdinov.codingtask.leetcode.macsubarray;

public class MaxSubarraySolution {
    public int maxSubArray(int... nums) {
        return maxSubArray(nums, 0, nums.length - 1);
    }
    
    private int maxSubArray(int[] nums, int start, int end) {
        if (end == start) {
            return nums[start];
        }
        
        final int mid = (int) Math.floor((start + end) / 2d);
        
        final int left = maxSubArray(nums, start, mid);
        final int right = maxSubArray(nums, mid + 1, end);
        final int cross = maxCross(nums, start, mid, end);
        
        return Math.max(cross, Math.max(left, right));
    }
    
    private int maxCross(int[] nums, int start, int mid, int end) {
        Integer leftSum = null;
        int sum = 0;
        
        for (int i = mid; start <= i; --i) {
            sum += nums[i];
            if (leftSum == null) {
                leftSum = sum;
            } else if (sum > leftSum) {
                leftSum = sum;
            }
        }
        
        sum = 0;
        Integer rightSum = null;
        for (int i = mid + 1; i <= end; ++i) {
            sum += nums[i];
            if (rightSum == null) {
                rightSum = sum;
            } else if (sum > rightSum) {
                rightSum = sum;
            }
        }
        if (leftSum == null) {
            return rightSum;
        } else if (rightSum == null) {
            return leftSum;
        } else {
            return leftSum + rightSum;
        }
    }
}
