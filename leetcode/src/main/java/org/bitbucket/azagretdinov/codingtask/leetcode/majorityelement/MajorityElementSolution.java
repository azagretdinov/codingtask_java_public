package org.bitbucket.azagretdinov.codingtask.leetcode.majorityelement;

import java.util.HashMap;
import java.util.Map;

public class MajorityElementSolution {
    public int majorityElement(int[] nums) {
        final Map<Integer, Integer> counter = new HashMap<>();
        final int m = (int) Math.ceil(nums.length/2d);
        
        for(int n: nums){
            Integer a = counter.get(n);
            if(a == null){
                a = 0;
            }
            a++;
            if(a == m){
                return n;
            }
            counter.put(n, a);
        }
        
        return -1;
    }
    
}
