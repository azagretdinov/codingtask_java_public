package org.bitbucket.azagretdinov.codingtask.leetcode.groupanagrams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupAnagramsSolution {
    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs == null || strs.length == 0) {
            return Collections.emptyList();
        }
        
        if (strs.length == 1) {
            final ArrayList<List<String>> r = new ArrayList<>();
            r.add(Collections.singletonList(strs[0]));
            return r;
        }
        
        final Map<String, List<String>> map = new HashMap<>();
        
        for (final String str : strs) {
            char chArray[] = str.toCharArray();
            Arrays.sort(chArray);
            
            String key = String.valueOf(chArray);
            
            List<String> group = map.get(key);
            if (group == null) {
                group = new ArrayList<>();
                map.put(key, group);
            }
            group.add(str);
        }
        
        return new ArrayList<>(map.values());
    }
    
}
