import java.util.Arrays;
import java.util.Scanner;

public class BiggerGreater {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println(nextGrateString(scanner.next().toCharArray()));
        }

    }

    private static String nextGrateString(char[] in) {
        int i = in.length - 1;
        while (0 < i && in[i] <= in[i - 1]) {
            i--;
        }
        if (i == 0) {
            return "no answer";
        }

        int x = in[i - 1];
        int minIndex = i;
        for (int j = i + 1; j < in.length; j++) {
            if (in[j] > x && in[j] < in[minIndex]) {
                minIndex = j;
            }
        }

        swap(in, i - 1, minIndex);

        Arrays.sort(in, i, in.length);

        return new String(in);
    }

    private static void swap(char[] in, int from, int to) {
        char t = in[from];
        in[from] = in[to];
        in[to] = t;
    }
}
