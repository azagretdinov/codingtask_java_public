import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class BiggerGreaterTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @Rule
    public final TextFromStandardInputStream systemInMock = TextFromStandardInputStream.emptyStandardInputStream();
    private String in;
    private String out;
    public BiggerGreaterTest(String in, String out) {
        this.in = in;
        this.out = out;
    }

    @Parameters(name = "{0} - {1}")
    public static Object[][] params() {
        return new Object[][]{
                new Object[]{"ab", "ba"},
                new Object[]{"bb", "no answer"},
                new Object[]{"hefg", "hegf"},
                new Object[]{"dhck", "dhkc"},
                new Object[]{"dkhc", "hcdk"},
        };
    }

    @Test
    public void shouldReturnGreaterWord() {
        systemInMock.provideLines("1",in);
        BiggerGreater.main(new String[0]);
        assertThat(systemOutRule.getLog()).isEqualTo(out);
    }


}
