import java.util.Scanner;

import static java.lang.Math.abs;

public class Main {

    private static final String[] digits =
            {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                    "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                    "eighteen", "nineteen"};
    private static final String[] tens =
            {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int number = in.nextInt();

        String[] r = new String[number];
        for (int i = 0; i < number; i++) {
            r[i] = intToText(in.next());
        }
        for (String a : r) {
            System.out.println(a);
        }
    }

    private static String intToText(String in) {
        if (in.length() == 0) {
            return "zero";
        }
        int intIn = Integer.parseInt(in);
        return intToText(intIn);
    }

    private static String intToText(int in) {
        if (isZero(in)) {
            return digits[0];
        }

        int positive = abs(in);

        String word = "";
        word += thousandToWords(positive);
        word += word.length() > 0 ? " " : "";
        word += hundredsToWords(positive % 1000);
        word += tensToWords(positive % 100);

        if (in < 0) {
            word = "minus " + word;
        }

        return word.trim();
    }

    private static String thousandToWords(int in) {
        String result = "";

        int thousand = in / 1000;
        int hundreds = in % 1000;

        if (thousand != 0) {
            result += digits[thousand] + " thousand";

            if (0 < hundreds && hundreds < 100) {
                result += " and";
            }
        }
        return result;
    }

    private static String hundredsToWords(int in) {
        String result = "";

        int hundreds = in / 100;
        int tensUnits = in % 100;

        if (hundreds != 0) {
            result += digits[hundreds] + " hundred";

            if (tensUnits != 0) {
                result += " and ";
            }
        }
        return result;
    }

    private static String tensToWords(int in) {

        String result = "";

        int inTens = in / 10;
        int inDigits = in % 10;

        if (inTens >= 2) {
            result += tens[inTens];
            if (inDigits != 0) {
                result += " " + digits[inDigits];
            }
        } else if (in != 0) { result += digits[in]; }

        return result;
    }

    private static boolean isZero(int in) {
        return in == 0;
    }
}
