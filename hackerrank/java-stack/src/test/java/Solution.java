import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Solution {


    @Test
    public void should_pass() {
        assertTrue(check("[]"));
        assertTrue(check("{}()"));
        assertTrue(check("({()})"));
        assertTrue(check("({()()}()[])"));

        assertFalse(check("{}("));
        assertFalse(check("("));
        assertFalse(check("]"));
        assertFalse(check("(})"));
    }


    public static boolean check(String input) {
        // code here
        final Stack<Character> myStack = new Stack<>();

        for (char c: input.toCharArray()){
            if (isOpenBrace(c)){
                myStack.push(c);
            }
            else {
                if(myStack.isEmpty()){
                    return false;
                }
                char c2 = myStack.pop();
                if (isMatch(c, c2))
                    return false;
            }

        }
        return myStack.isEmpty();
    }

    private static boolean isMatch(char c, char c2) {
        if(c == ']' && c2 != '['){
            return true;
        }
        if(c == '}' && c2 != '{'){
            return true;
        }
        if(c == ')' && c2 != '('){
            return true;
        }
        return false;
    }

    private static boolean isOpenBrace(char c) {
        return c=='[' || c=='{' || c=='(';
    }

}
