package org.bitbucket.azagretdinov.algorithms.frontbacksplit;


import org.bitbucket.azagretdinov.algorithms.frontbacksplit.LinkedList.Node;

public class FrontBackSpliter {
    
    public void split(LinkedList in, LinkedList a, LinkedList b){
        if (in.isEmpty()){
            return;
        }
    
        Node slow = in.head;
        Node fast = in.head.next;
        
        while (fast != null){
            fast = fast.next;
            if(fast != null){
                slow = slow.next;
                fast = fast.next;
            }
        }
        
        a.head = in.head;
        b.head = slow.next;
        slow.next = null;
    }
    
}
