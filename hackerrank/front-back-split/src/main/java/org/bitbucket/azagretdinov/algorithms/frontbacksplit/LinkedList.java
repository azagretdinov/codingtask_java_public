package org.bitbucket.azagretdinov.algorithms.frontbacksplit;

public class LinkedList {
    
    Node head = null;
    
    public void add(int data) {
        Node node = new Node(data);
        
        if(head == null){
            head = node;
        }else {
            Node p = head;
            while (p.next != null){
                p = p.next;
            }
            p.next = node;
        }
    }
    
    public boolean isEmpty() {
        return head == null;
    }
    
    public static class Node {
        int data;
        Node next;
    
        public Node(int data) {
            this.data = data;
        }
    }
}
