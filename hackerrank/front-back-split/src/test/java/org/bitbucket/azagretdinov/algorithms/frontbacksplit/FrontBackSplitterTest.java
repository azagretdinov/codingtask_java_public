package org.bitbucket.azagretdinov.algorithms.frontbacksplit;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FrontBackSplitterTest {
    
    private LinkedList in;
    private LinkedList a;
    private LinkedList b;
    private FrontBackSpliter objectUnderTest;
    
    @Before
    public void setUp() throws Exception {
        in = new LinkedList();
        a = new LinkedList();
        b = new LinkedList();
        objectUnderTest = new FrontBackSpliter();
    }
    
    @Test
    public void shouldReturnEmptyListIfInEmpty() {
    
        new FrontBackSpliter().split(in, a, b);
        
        assertThat(a.head).isNull();
        assertThat(b.head).isNull();
    }
    
    @Test
    public void shouldPutElementToFirstListIfOnlyOneElement() {
        in.add(1);
        
        objectUnderTest.split(in, a, b);
        
        assertThat(a.head.data).isEqualTo(1);
        assertThat(b.head).isNull();
    }
    
    @Test
    public void shouldSplitListWithTwoElements() {
        in.add(1);
        in.add(2);
    
        objectUnderTest.split(in, a, b);
    
        assertThat(a.head.data).isEqualTo(1);
        assertThat(b.head.data).isEqualTo(2);
    }
    
    @Test
    public void shouldSplitListWithTwoOddElements() {
        in.add(1);
        in.add(2);
        in.add(3);
        
        objectUnderTest.split(in, a, b);
        
        assertThat(a.head.data).isEqualTo(1);
        assertThat(a.head.next.data).isEqualTo(2);
        assertThat(b.head.data).isEqualTo(3);
    }
    
    @Test
    public void shouldSplitListWithFourElements() {
        in.add(1);
        in.add(2);
        in.add(3);
        in.add(4);
        
        objectUnderTest.split(in, a, b);
        
        assertThat(a.head.data).isEqualTo(1);
        assertThat(a.head.next.data).isEqualTo(2);
        assertThat(b.head.data).isEqualTo(3);
        assertThat(b.head.next.data).isEqualTo(4);
    }
    
}