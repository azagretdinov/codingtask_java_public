package org.bitbucket.azagretdinov.hackerrank.timeconversion;

/**
 *
 */
public class CircularArrayRotation {
    public int rotateAndQuery(int[] a, int q, int k) {
        final int rotation = k % a.length;
        int index = q - rotation;
        if (index < 0){
            index = a.length + index;
        }
        return a[index];
    }
}
