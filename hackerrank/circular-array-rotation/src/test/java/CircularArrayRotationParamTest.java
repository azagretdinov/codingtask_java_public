import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.azagretdinov.hackerrank.timeconversion.CircularArrayRotation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class CircularArrayRotationParamTest {


    private final int[] a;
    private final int in;
    private final int out;

    @Parameters(name = "{1} - {2}")
    public static Object[][] params() throws IOException {
        String arrayAsString =  FileUtils.readFileToString(new File("target/classes/inArray.txt"));
        String[] strings = StringUtils.split(arrayAsString, ' ');
        int[] a = new int[strings.length];
        for (int i = 0; i < strings.length; i++) {
            a[i] = Integer.parseInt(strings[i]);
        }
        List<String> in = FileUtils.readLines(new File("target/classes/input.txt"));
        List<String> out = FileUtils.readLines(new File("target/classes/output.txt"));
        Object[][] result = new Object[in.size()][3];

        for (int i = 0; i < in.size(); i++) {
            Object[] params = result[i];
            params[0] = a;
            params[1] = in.get(i);
            params[2] = out.get(i);
        }

        return result;
    }

    private CircularArrayRotation circularArrayRotation;


    public CircularArrayRotationParamTest(int[] a, String in, String out){
        this.a = a;
        this.in = Integer.parseInt(in);
        this.out = Integer.parseInt(out);
    }

    @Before
    public void setUp() throws Exception {
        circularArrayRotation = new CircularArrayRotation();
    }


    @Test
    public void shouldReturnExpected() {
        assertThat(circularArrayRotation.rotateAndQuery(a, in, 72)).isEqualTo(out);
    }
}
