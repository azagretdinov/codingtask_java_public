import org.bitbucket.azagretdinov.hackerrank.timeconversion.CircularArrayRotation;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CircularArrayRotationTest {

    private CircularArrayRotation circularArrayRotation;

    @Before
    public void setUp() throws Exception {
        circularArrayRotation = new CircularArrayRotation();
    }

    @Test
    public void shouldReturnOneForArraysWithOne() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1}, 0, 0);
        assertThat(m).isEqualTo(1);
    }

    @Test
    public void shouldReturnOneForArraysWithOneAndTwoAndZeroRotation() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2}, 0, 0);
        assertThat(m).isEqualTo(1);
    }

    @Test
    public void shouldReturnTwoForArraysWithOneAndTwoAndZeroRotation() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2}, 1, 0);
        assertThat(m).isEqualTo(2);
    }

    @Test
    public void shouldReturnTwoForArraysWithOneAndTwoAndOneRotation() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2}, 0, 1);
        assertThat(m).isEqualTo(2);
    }

    @Test
    public void shouldReturnOneForArraysWithOneAndTwoAndTwoRotation() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2}, 0, 2);
        assertThat(m).isEqualTo(1);
    }

    @Test
    public void shouldReturnTwoForArraysWithOneAndTwoAndThreeAndTwoRotation() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2 ,3}, 0, 2);
        assertThat(m).isEqualTo(2);
    }

    @Test
    public void shouldReturnTwoForArraysWithOneAndTwoAndThreeAndFiveRotationAndQZero() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2 ,3}, 0, 5);
        assertThat(m).isEqualTo(2);
    }

    @Test
    public void shouldReturnThreeForArraysWithOneAndTwoAndThreeAndFiveRotationAndQOne() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2 ,3}, 1, 2);
        assertThat(m).isEqualTo(3);
    }

    @Test
    public void shouldReturnOneForArraysWithOneAndTwoAndThreeAndFiveRotationAndQTwo() throws Exception {
        int m = circularArrayRotation.rotateAndQuery(new int[]{1, 2 ,3}, 2, 2);
        assertThat(m).isEqualTo(1);
    }
}
