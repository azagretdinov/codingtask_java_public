package org.bitbucket.azagretdinov.javasort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());

        List<Student> studentList = new ArrayList<Student>();
        while (testCases > 0) {
            int id = in.nextInt();
            String fname = in.next();
            double cgpa = in.nextDouble();

            Student st = new Student(id, fname, cgpa);
            studentList.add(st);
            sort(studentList);
            testCases--;
        }

        for (Student st : studentList) {
            System.out.println(st.getFname());
        }
    }

    public static void sort(List<Student> studentList) {
        studentList.sort(
                Comparator.comparing(Student::getCgpa).reversed().thenComparing(Student::getFname)
        );
    }
}