/**
 *
 */
public class GFGMain {

    class Node {
        int data;
        Node left, right;

        Node(int item) {
            data = item;
            left = right = null;
        }
    }

    class GfG {
        void modify(Node root) {
            int rightSum = rightSum(root.right, 0);
            root.data += rightSum;
        }

        int rightSum(Node node, int sum) {
            if (node == null) {
                return sum;
            }
            int rightSum = rightSum(node.right, sum);
            node.data += rightSum;
            return node.data;
        }
    }

}
