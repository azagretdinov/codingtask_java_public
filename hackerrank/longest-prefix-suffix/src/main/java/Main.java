import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int number = in.nextInt();

        int[] r = new int[number];
        for (int i = 0; i < number; i++) {
            r[i] = longerPrefixSuffix(in.next());
        }
        for (int a : r) {
            System.out.println(a);
        }
    }

    private static int longerPrefixSuffix(String in) {
        final int length = in.length();
        if (length < 1) {
            return 0;
        }
        final char startChar = in.charAt(0);
        int maxLength = 0;
        int suffixStart = in.indexOf(startChar, 1);
        while (suffixStart > 0 && suffixStart + maxLength < length) {
            if (in.charAt(suffixStart + maxLength) == in.charAt(maxLength)) {
                maxLength++;
            } else {
                maxLength = 0;
                suffixStart = in.indexOf(startChar, suffixStart + 1);
            }
        }

        return maxLength;
    }

    private static void assertEquals(int actual, int expected, String inValue) {
        if (actual != expected) {
            System.err.print(String.format("Failed: %s. ", inValue));
            System.err.println(String.format("Expected %s, but actual %s", expected, actual));
        } else {
            System.out.println(String.format("Passed: %s", inValue));
        }
    }
}
