package org.bitbucket.azagretdinov.hackerrank;

import java.util.LinkedList;
import java.util.Queue;

public class GfG {
    void connect(Node root) {
        if (root == null) {
            return;
        }

        Queue<Node> queue = new LinkedList<Node>();
        queue.add(root);
        queue.add(null);

        while (!queue.isEmpty()) {
            Node node = queue.poll();

            if (node != null) {
                node.nextRight = queue.peek();
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
            } else if (queue.peek() != null) {
                queue.add(null);
            }
        }
    }
}
