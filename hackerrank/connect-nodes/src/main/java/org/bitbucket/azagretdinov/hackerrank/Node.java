package org.bitbucket.azagretdinov.hackerrank;

public class Node {

    int data;
    Node left;
    Node right;
    Node nextRight;

    public Node(int item)
    {
        data = item;
        left = null;
        right = null;
        nextRight = null;
    }

    @Override
    public String toString() {
        return "Node{" +
                       "right=" + right +
                       ", left=" + left +
                       ", data=" + data +
                       ", nextRight=" + nextRight +
                       '}';
    }
}
