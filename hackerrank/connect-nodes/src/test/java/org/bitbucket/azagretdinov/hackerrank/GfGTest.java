package org.bitbucket.azagretdinov.hackerrank;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GfGTest {

    private GfG objectUnderTest;

    @Before
    public void setUp() throws Exception {
        objectUnderTest = new GfG();
    }

    @Test
    public void shouldReturnNodeWithLinkToNullForOneNodeTree() {
        Node node = new Node(10);
        objectUnderTest.connect(node);
        assertThat(node.nextRight).isNull();
    }

    @Test
    public void shouldReturnTreeWithLeftSubtreeLinkedToRight(){
        Node root = new Node(10);
        Node left = new Node(9);
        Node right = new Node(11);
        root.left = left;
        root.right = right;
        objectUnderTest.connect(root);
        assertThat(root.left.nextRight).isSameAs(right);
    }


    @Test
    public void geeksforgeeksFailTest(){
        Node root = new Node(1);
        Node left = new Node(2);
        Node right = new Node(3);

        root.left = left;
        root.right = right;

        final Node leftLeftLeaf = new Node(4);
        final Node leftRightLeaf = new Node(5);

        left.left = leftLeftLeaf;
        left.right = leftRightLeaf;

        final Node rightLeftLeaf = new Node(6);
        final Node rightRightLeaf = new Node(7);

        right.left = rightLeftLeaf;
        right.right = rightRightLeaf;

        objectUnderTest.connect(root);

        assertThat(root.left.nextRight).isSameAs(right);
        assertThat(root.left.left.nextRight).isSameAs(leftRightLeaf);
        assertThat(root.left.right.nextRight).isSameAs(rightLeftLeaf);
        assertThat(root.right.left.nextRight).isSameAs(rightRightLeaf);
    }
}