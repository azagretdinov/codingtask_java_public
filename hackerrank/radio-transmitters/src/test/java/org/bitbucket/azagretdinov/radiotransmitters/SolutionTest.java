package org.bitbucket.azagretdinov.radiotransmitters;


import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class SolutionTest {
    
    @Rule
    public SystemOutRule out = new SystemOutRule().enableLog().muteForSuccessfulTests();
    
    @Rule
    public TextFromStandardInputStream in = TextFromStandardInputStream.emptyStandardInputStream();
    
    @Test
    public void should_return_one_if_one_size_array_and_range_one() throws IOException {
        in.provideLines("1 1", "1");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("1");
    }
    
    @Test
    public void should_return_one_if_two_size_array_and_range_one() throws IOException {
        in.provideLines("2 1", "1 2");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("1");
    }
    
    @Test
    public void should_return_one_if_three_size_array_and_range_one() throws IOException {
        in.provideLines("3 1", "1 2 3");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("1");
    }
    
    @Test
    public void should_return_two_if_four_size_array_and_range_one() throws IOException {
        in.provideLines("4 1", "1 2 3 4");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("2");
    }
    
    @Test
    public void should_return_two_if_three_size_array_with_hole_and_range_one() throws IOException {
        in.provideLines("2 1", "1 3");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("2");
    }
    
    @Test
    public void should_return_one_if_three_size_array_with_hole_and_range_two() throws IOException {
        in.provideLines("2 2", "1 3");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("1");
    }
    
    @Test
    public void should_return_two_if_five_size_array_and_range_one() throws IOException {
        in.provideLines("5 1", "1 2 3 4 5");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("2");
    }
    
    @Test
    public void should_return_two_if_eighte_size_array_with_hole_and_range_two() throws IOException {
        in.provideLines("8 2", "7 2 4 6 5 9 12 11");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("3");
    }
    
    
    @Test
    public void should_return_three_if_three_size_array_with_hole_and_range_one() throws IOException {
        in.provideLines("3 1", "1 3 5");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("3");
    }
    
    
    @Test
    public void should_return_two_if_seven_size_array_and_range_one_and_duplicates() throws IOException {
        in.provideLines("7 1", "1 2 3 3 4 5 5");
        
        Solution.main(new String[0]);
        
        assertThat(out.getLog()).isEqualTo("2");
    }
}