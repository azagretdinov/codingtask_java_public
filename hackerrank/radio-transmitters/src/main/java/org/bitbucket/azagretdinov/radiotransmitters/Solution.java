package org.bitbucket.azagretdinov.radiotransmitters;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Solution {
    
    public static void main(String[] args) throws IOException {
        
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        final String[] nk = reader.readLine().split(" ");
        final int n = Integer.parseInt(nk[0]);
        final int k = Integer.parseInt(nk[1]);
        
        final int[] x = new int[n];
        final String[] xs = reader.readLine().split(" ");
        
        for (int i = 0; i < n; i++) {
            x[i] = Integer.parseInt(xs[i]);
        }
        
        Arrays.sort(x);
        
        int count = calculateCount(n, k, x);
        
        System.out.print(count);
    }
    
    private static int calculateCount(final int n, final int k, int[] x) {
        if (x.length == 0) {
            return 0;
        }
        
        Map<Integer, Integer> map = new HashMap<>();
        
        for (int i = 0; i < x.length; i++) {
            map.put(x[i], i);
        }
        
        int i = 0;
        
        int count = 0;
        
        while (i < n) {
            int currentHome = x[i];
            Integer next = i;
            int candidate = currentHome + k;
            
            while (candidate >= currentHome && ((next = map.get(candidate)) == null)) {
                candidate--;
            }
            
            count++;
            
            i = next + 1;
            
            while (i < n && x[i] - x[next] <= k){
                i++;
            }
        }
        return count;
    }
    
}
