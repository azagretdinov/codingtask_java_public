package org.bitbucket.azagretdinov.hackerrank.timeconversion;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeConversionTest {

    @Test
    public void shouldReturn13For1AM() {
        TimeConversion timeConversion = new TimeConversion();
        assertThat(timeConversion.convert("01:00:45PM")).isEqualTo("13:00:45");
    }

    @Test
    public void shouldReturn1For12AM() {
        TimeConversion timeConversion = new TimeConversion();
        assertThat(timeConversion.convert("01:00:45AM")).isEqualTo("01:00:45");
    }

    @Test
    public void shouldReturn0For12AM() {
        TimeConversion timeConversion = new TimeConversion();
        assertThat(timeConversion.convert("12:00:45AM")).isEqualTo("00:00:45");
    }

    @Test
    public void shouldReturn12For12PM() {
        TimeConversion timeConversion = new TimeConversion();
        assertThat(timeConversion.convert("12:45:54PM")).isEqualTo("12:45:54");
    }

}
