package org.bitbucket.azagretdinov.hackerrank.timeconversion;

/**
 *
 */
public class TimeConversion {
    public String convert(String time12) {
        int hour12 = Integer.parseInt(time12.substring(0, 2));
        int hour24 = getHour24(time12, hour12);
        String prefix = hour24 < 10 ? "0"  : "";
        return  prefix +  hour24 + time12.substring(2, time12.length() - 2);
    }

    private int getHour24(String time12, int hour12) {
        char pmAm = time12.charAt(time12.length() - 2);
        int pmAmShift = 0;
        if (pmAm == 'A' && hour12 == 12){
            hour12 = 0;
        }else if (pmAm == 'P' && hour12 == 12){
            hour12 = 12;
        }else if (pmAm == 'P'){
            pmAmShift = 12;
        }
        return hour12 + pmAmShift;
    }
}
