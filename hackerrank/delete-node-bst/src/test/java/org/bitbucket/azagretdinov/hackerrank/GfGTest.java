package org.bitbucket.azagretdinov.hackerrank;


import org.bitbucket.azagretdinov.hackerrank.GfG.Node;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class GfGTest {

    @Parameters
    public static Object[][] params() {
        return new Object[][]{
                {
                        81,
                        new int[]{2, 81, 87, 42, 66, 90, 45}
                }
        };
    }

    private final GfG gfG;
    private int keyToDelete;
    private Node tree;

    public GfGTest(int keyToDelete, int[] tree) {
        gfG = new GfG();
        this.keyToDelete = keyToDelete;
        this.tree = createTree(tree);
    }

    private Node createTree(int[] tree) {
        Node root = new Node(tree[0]);
        for (int i = 1; i < tree.length; i++) {
            gfG.addNode(root, tree[i]);
        }
        return root;
    }

    @Test
    public void delete() {
        gfG.deleteNode(tree,keyToDelete);
        assertThat(gfG.search(tree,keyToDelete)).isNull();
        System.out.println(gfG.toString(tree));
    }
}