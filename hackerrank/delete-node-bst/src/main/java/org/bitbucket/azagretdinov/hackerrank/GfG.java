package org.bitbucket.azagretdinov.hackerrank;


import java.util.StringJoiner;

public class GfG {

    Node deleteNode(Node root, int key) {
        SearchResult noteToDelete = search(root, null, key);
        return deleteNode(root, noteToDelete);
    }

    SearchResult search(Node node, Node parent, int key) {
        while (node != null && node.key != key) {
            parent = node;
            if (key < node.key) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        return new SearchResult(node, parent);
    }

    Node deleteNode(Node root, SearchResult result) {
        Node nodeToDelete = result.node;
        Node parent = result.parent;
        if (nodeToDelete.left == null) {
            return replaceSubTree(root, parent, nodeToDelete, nodeToDelete.right);
        } else if (nodeToDelete.right == null) {
            return replaceSubTree(root, parent, nodeToDelete, nodeToDelete.left);
        }
        SearchResult newSubtree = findMinimum(nodeToDelete.right, nodeToDelete);

        if (newSubtree.parent != nodeToDelete) {
            replaceSubTree(root, newSubtree.parent, newSubtree.node, newSubtree.node.right);
            newSubtree.node.right = nodeToDelete.right;
        }
        newSubtree.node.left = nodeToDelete.left;

        return replaceSubTree(root, parent, nodeToDelete, newSubtree.node);
    }

    private SearchResult findMinimum(Node node, Node parent) {
        if (node.left == null) {
            return new SearchResult(node, parent);
        } else {
            return findMinimum(node.left, node);
        }
    }

    private Node replaceSubTree(Node root, Node parent, Node nodeToReplace, Node node) {
        if (parent == null) {
            return node;
        }
        if (parent.left == nodeToReplace) {
            parent.left = node;
        } else {
            parent.right = node;
        }
        return root;
    }

    private class SearchResult {
        private final Node node;
        private final Node parent;

        public SearchResult(Node node, Node parent) {
            super();
            this.node = node;
            this.parent = parent;
        }
    }

    void addNode(Node root, int nodeToAdd) {
        Node candidate = root;
        Node x = root;

        while (x != null) {
            candidate = x;
            if (nodeToAdd < x.key) {
                x = x.left;
            } else {
                x = x.right;
            }
        }

        if (nodeToAdd < candidate.key) {
            candidate.left = new Node(nodeToAdd);
        } else {
            candidate.right = new Node(nodeToAdd);
        }
    }

    public String toString(Node tree) {
        if (tree == null){
            return null;
        }
        StringJoiner joiner = new StringJoiner(", ");
        String v = toString(tree.left);
        if (v != null) {
            joiner.add(v);
        }
        joiner.add(String.valueOf(tree.key));
        v = toString(tree.right);
        if (v != null) {
            joiner.add(v);
        }
        return joiner.toString();
    }

    Node search(Node node, int key) {
        SearchResult searchResult = search(node, null, key);
        if (searchResult != null) {
            return searchResult.node;
        }
        return null;
    }

    public static class Node {
        int key;

        Node left, right;

        public Node(int item) {
            key = item;
            left = right = null;
        }

        @Override
        public String toString() {
            String left = this.left == null ? "null" : String.valueOf(this.left.key);
            String right = this.right == null ? "null" : String.valueOf(this.right.key);
            return "Node{" +
                           "key=" + key +
                           ", left=" + left +
                           ", right=" + right +
                           '}';
        }
    }
}
