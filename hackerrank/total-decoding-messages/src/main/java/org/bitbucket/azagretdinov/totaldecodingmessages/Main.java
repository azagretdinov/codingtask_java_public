package org.bitbucket.azagretdinov.totaldecodingmessages;

public class Main {

    private static byte shift = 96;

    public static void main(String[] args) {
        assertThatTotalDecoding("", 0);
        assertThatTotalDecoding("1", 1);
        assertThatTotalDecoding("123", 3);
        assertThatTotalDecoding("2563", 2);
    }

    private static void assertThatTotalDecoding(String in, int expected) {
        int actual = totalDecoding(in);
        assertEquals(actual, expected, in);
    }

    private static int totalDecoding(String in) {
        return totalDecoding(in.getBytes());
    }

    private static int totalDecoding(byte[] in) {
        return  totalDecoding(in, 0, in.length - 1);
    }

    private static int totalDecoding(byte[] in, int start, int end) {
        int lenght = end - start;
        if (lenght == 2){
            return twoDigitsCase(in, start, end);
        }
        int mid = lenght / 2;
        int left = totalDecoding(in, 0, mid -1) - 1;
        int right = totalDecoding(in, mid, end) - 1;
        return left + right + twoDigitsCase(in, mid, mid + 1);


    }

    private static int twoDigitsCase(byte[] in, int start, int end) {
        int x = in[start] - shift;
        int y = in[end] - shift;
        if (x == 0){
            return 0;
        }
        return x;
    }


    private static void assertEquals(int actual, int expected, String inValue) {
        if (actual != expected) {
            System.err.print(String.format("Failed: %s. ", inValue));
            System.err.println(String.format("Expected %s, but actual %s", expected, actual));
        } else {
            System.out.println(String.format("Passed: %s", inValue));
        }
    }
}
