package org.bitbucket.azagretdinov.hackerrank;


import org.bitbucket.azagretdinov.hackerrank.GfG.Node;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GfGTest {

    private GfG gfG;

    @Before
    public void setUp() throws Exception {
        gfG = new GfG();
    }

    @Test
    public void shouldReturnZeroIfTreeEmpty() {
        assertThat(gfG.findDist(null, 1, 2)).isEqualTo(0);
    }

    @Test
    public void shouldReturnZeroIfElementNotExistEmpty() {
        assertThat(gfG.findDist(new Node(10), 1, 2)).isEqualTo(0);
    }

    @Test
    public void shouldReturnOneIfElementIsLeftChildOfRoot() {
        Node root = new Node(10);
        root.left = new Node(5);
        assertThat(gfG.findDist(root, 10, 5)).isEqualTo(1);
    }

    @Test
    public void shouldReturnTwoIfElementIsChildrenOfRoot() {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        assertThat(gfG.findDist(root, 2, 3)).isEqualTo(2);
    }

    @Test
    public void shouldReturnOneIfElementIsChildrenOfSubTree() {
        Node root = new Node(10);
        root.left = new Node(20);
        root.right = new Node(30);
        root.left.left = new Node(40);
        root.left.right = new Node(60);
        assertThat(gfG.findDist(root, 20, 60)).isEqualTo(1);
    }

    @Test
    public void shouldReturnFourIfElementIsChildrenOfSubTree() {
        Node root = new Node(1);
        root.right = new Node(2);
        root.right.right = new Node(4);
        root.right.right.right = new Node(5);
        root.right.right.right.right = new Node(6);
        root.right.right.right.right.right = new Node(7);
        root.right.right.right.right.right.left = new Node(8);

        assertThat(gfG.findDist(root, 2, 7)).isEqualTo(4);
    }
    
}