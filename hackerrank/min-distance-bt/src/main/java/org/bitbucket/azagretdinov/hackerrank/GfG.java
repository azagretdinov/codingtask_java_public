package org.bitbucket.azagretdinov.hackerrank;


public class GfG {

    int findDist(Node root, int a, int b) {

        SearchResult result = new SearchResult();
        result.distanceToRootA = -1;
        result.distanceToRootB = -1;
        Node node = searchCommonNode(root, a, b, result, 1);

        if (result.distanceToRootA != -1 && result.distanceToRootB != -1) {
            return result.distanceAToB;
        }

        if (result.distanceToRootA != -1) {
            return findDistance(node, b, 0);
        }

        if (result.distanceToRootB != -1) {
            return findDistance(node, a, 0);
        }

        return 0;
    }

    private int findDistance(Node node, int value, int level) {

        if (node == null) {
            return 0;
        }

        if (node.data == value) {
            return level;
        }

        int r = findDistance(node.left, value, level + 1);

        if (r == 0) {
            r = findDistance(node.right, value, level + 1);
        }
        return r;
    }

    private Node searchCommonNode(Node node, int a, int b, SearchResult result, int level) {
        if (node == null) {
            result.distanceAToB = 0;
            return null;
        }

        if (node.data == a) {
            result.distanceToRootA = level;
            result.nodeA = node;
            return node;
        }

        if (node.data == b) {
            result.distanceToRootB = level;
            result.nodeB = node;
            return node;
        }

        Node left = searchCommonNode(node.left, a, b, result, level + 1);
        Node right = searchCommonNode(node.right, a, b, result, level + 1);

        if (left != null && right != null) {
            result.distanceAToB = result.distanceToRootA + result.distanceToRootB - 2 * level;
            return node;
        }

        return left != null ?  left : right;
    }


    static class SearchResult {
        int distanceToRootA;
        int distanceToRootB;
        Integer distanceAToB;
        Node nodeA;
        Node nodeB;
    }

    static class Node {
        int data;
        Node left, right;

        Node(int item) {
            data = item;
            left = right = null;
        }
    }
}
