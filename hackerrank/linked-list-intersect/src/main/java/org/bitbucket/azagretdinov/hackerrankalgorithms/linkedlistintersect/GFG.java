package org.bitbucket.azagretdinov.hackerrankalgorithms.linkedlistintersect;

public class GFG {
    Intersect llist3 = new Intersect(); // object of LinkedList having Intersection of two LinkedLists
    
    // Function  to find Intersection of two LinkedLists
    void getIntersection(Node head1, Node head2) {
        Node a = head1,
                b = head2,
                c = null;
        
        while (a != null && b != null) {
            while (a != null && a.data < b.data) { a = a.next; }
    
            if (a != null) { while (b != null && b.data < a.data) { b = b.next; } }
            
            if (a != null && b != null && a.data == b.data) {
                Node next = new Node(a.data);
                if (c == null) {
                    c = next;
                    llist3.head = c;
                } else {
                    c.next = next;
                    c = c.next;
                }
                
                a = a.next;
                b = b.next;
                
            }
        }
    }
}

class Node {
    int data;
    Node next;
    
    Node(int d) {
        data = d;
        next = null;
    }
}

class Intersect {
    Node head;  // head of list
    
}