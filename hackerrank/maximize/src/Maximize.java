public class Maximize {

    public static void main(String[] args) {
        String maximize = maximize(2, 1, 2, 4, 3);
        assertThatEquals(maximize, "4123");

        maximize = maximize(2, 5, 1, 2, 3, 5);
        assertThatEquals(maximize, "53125");

        maximize = maximize(3, 5, 1, 2, 3, 5);
        assertThatEquals(maximize, "55123");

        maximize = maximize(1, 1, 2, 3);
        assertThatEquals(maximize, "213");

        maximize = maximize(3, 5, 1, 2,0 ,3, 5);
        assertThatEquals(maximize, "531205");

        maximize = maximize(5, 1, 2, 3);
        assertThatEquals(maximize, "321");
    }

    private static void assertThatEquals(String maximize, String expected) {
        if (!maximize.equals(expected)) {
            throw new AssertionError(String.format("Expected `%s`, but actually got `%s`.", expected, maximize));
        }
    }

    private static String maximize(int maxSwapCount, int... a) {
        int i = 0;
        int swapCount = maxSwapCount;
        while (i < a.length && 0 < swapCount) {
            int j = maxIndex(a, i, swapCount);
            while (j > i && swapCount > 0) {
                swap(a, j, j - 1);
                swapCount--;
                j--;
            }
            i++;
        }
        StringBuilder sb = new StringBuilder();
        for (int a1 : a) {
            sb.append(a1);
        }
        return sb.toString();
    }

    private static int maxIndex(int[] a, int start, int length) {
        int maxIndex = start;
        int maxElement = a[start];
        int i = start + 1;
        while (i <= start + length && i < a.length) {
            if (a[i] > maxElement) {
                maxElement = a[i];
                maxIndex = i;
            }
            i++;
        }
        return maxIndex;
    }


    private static void swap(int[] a, int from, int to) {
        if (to - from > 1) {
            throw new IllegalArgumentException();
        }
        int t = a[from];
        a[from] = a[to];
        a[to] = t;
    }


}
