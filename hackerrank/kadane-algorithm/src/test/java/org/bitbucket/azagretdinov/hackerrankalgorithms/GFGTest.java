package org.bitbucket.azagretdinov.hackerrankalgorithms;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class GFGTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    @Rule
    public final TextFromStandardInputStream systemInMock = TextFromStandardInputStream.emptyStandardInputStream();

    private final String size;
    private final String array;
    private String out;

    public GFGTest(String size, String array, String out) {
        this.size = size;
        this.array = array;
        this.out = out;
    }

    @Parameters(name = "{1} / {2}")
    public static Object[][] params() {
        return new Object[][]{
//                new Object[]{
//                        "4", // size
//                        "-1 -2 -3 -4", // array
//                        "-1\n" // out
//                },
//                new Object[]{
//                        "85",
//                        "34 -46 72 -43 69 32 63 -93 -17 11 -65 -33 -52 -25 38 -77 42 54 11 41 75 -41 -75 -79 -30 -74 34 -95 83 -50 98 -21 1 93 34 -63 34 56 93 76 5 62 48 -19 0 -87 -59 -45 -45 42 -38 -89 -23 -76 -22 52 -57 -4 -27 -60 13 -25 -28 -82 -90 -83 32 12 -5 69 -69 -60 -12 -15 -10 -3 89 90 45 53 14 -49 40 -56 -42",
//                        "549\n"
//                },
                new Object[]{
                        "11",
                        "-28 28 -66 -59 18 3 -33 -35 38 -19 -43",
                        "38\n"
                }
        };
    }

    @Test
    public void shouldReturnGreaterWord() {
        systemInMock.provideLines("1", size, array);
        GFG.main(new String[0]);
        assertThat(systemOutRule.getLog()).isEqualTo(out);
    }

}