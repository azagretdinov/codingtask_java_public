package org.bitbucket.azagretdinov.hackerrankalgorithms;

import java.util.Scanner;

/**
 *
 */
public class GFG {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int tcCount = s.nextInt();
        for (int i = 0; i < tcCount; i++) {
            int[] a = readArray(s, s.nextInt());
            System.out.println(maxSubArraySum(a, 0, a.length - 1));
        }
    }

    private static int maxSubArraySum(int[] a, int low, int high) {
        if (high - low == 1) {
            return a[low];
        }

        int mid = (low + high) / 2;

        int leftSum = maxSubArraySum(a, low, mid);
        int rightSum = maxSubArraySum(a, mid, high);
        int crossSum = maxCrossingSubArray(a, low, high, mid);

        if (leftSum >= rightSum && leftSum >= crossSum) {
            return leftSum;
        } else if (rightSum >= leftSum && rightSum >= crossSum) {
            return rightSum;
        } else {
            return crossSum;
        }
    }

    private static Integer maxCrossingSubArray(int[] a, int low, int high, int mid) {
        Integer leftSum = Integer.MIN_VALUE;
        int sum = 0;

        for (int i = mid; low <= i; i--) {
            sum += a[i];
            if (sum > leftSum) {
                leftSum = sum;
            }
        }

        Integer rightSum = Integer.MIN_VALUE;
        sum = 0;

        for (int i = mid + 1; i <= high; i++) {
            sum += a[i];
            if (sum > rightSum) {
                rightSum = sum;
            }
        }

        if (leftSum == Integer.MIN_VALUE || rightSum == Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }

        return leftSum + rightSum;
    }

    private static int[] readArray(Scanner s, int size) {
        int[] a = new int[size];
        for (int i = 0; i < size; i++) {
            a[i] = s.nextInt();
        }
        return a;
    }

}
