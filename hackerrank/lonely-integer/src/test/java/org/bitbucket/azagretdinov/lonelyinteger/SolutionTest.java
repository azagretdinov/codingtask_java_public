package org.bitbucket.azagretdinov.lonelyinteger;


import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class SolutionTest {
    
    @Rule
    public TextFromStandardInputStream inputStream = TextFromStandardInputStream.emptyStandardInputStream();
    
    @Rule
    public SystemOutRule systemOutRule = new SystemOutRule();
    
    
    @Test
    public void should_return_lonely_integer() {
        inputStream.provideLines("5","0 0 1 2 1");
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLogWithNormalizedLineSeparator()).isEqualTo("2");
    }
    
    @Test
    public void should_return_single_integer() {
        inputStream.provideLines("1","1");
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLogWithNormalizedLineSeparator()).isEqualTo("1");
    }
    
}