package org.bitbucket.azagretdinov.lonelyinteger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        int n = Integer.parseInt(in.readLine());
        String[] a = in.readLine().split(" ");
        
        int x = 0;
        for (int i = 0; i < n; i++) {
            int y = Integer.parseInt(a[i]);
            x = x ^ y;
        }
        System.out.print(x);
    }
    
}
