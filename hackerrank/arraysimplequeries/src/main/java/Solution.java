import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    public static void main(String[] args) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String[] input = in.readLine().split(" ");
        
        int size = Integer.parseInt(input[0]);
        int qCount = Integer.parseInt(input[1]);
        
        String[] a = in.readLine().split(" ");
        
        for(int index = 0; index < qCount; index++){
            input = in.readLine().split(" ");
            
            int i = Integer.parseInt(input[1]) - 1;
            int j = Integer.parseInt(input[2]) - 1;
            
            if("1".equals(input[0])){
                applyCommand1(a, i, j);
            }else if("2".equals(input[0])){
                applyCommand2(a, i, j);
            }
        }
        
        int r = Integer.parseInt(a[0]) - Integer.parseInt(a[size - 1]);
        if(r < 0){
            r = -r;
        }
        System.out.println(r);
    
        String out = "";
        
        for (String anA : a) {
            out += anA;
            out += " ";
        }
        
        out = out.trim();
        
        System.out.print(out);
    }
    
    private static void applyCommand1(String[] a, int i, int j){
        int l = j - i + 1;
        
        String[] t = new String[l];
        System.arraycopy(a, i, t, 0, l);
        
        String[] t1 = new String[i];
        System.arraycopy(a, 0, t1, 0, t1.length);
        
        System.arraycopy(t, 0, a, 0, t.length);
        System.arraycopy(t1, 0, a, j - t1.length + 1, t1.length);
    }
    
    private static void applyCommand2(String[] a, int i, int j){
        int l = j - i + 1;
    
        String[] t = new String[l];
        System.arraycopy(a, i, t, 0, l);
    
        String[] t1 = new String[a.length - j - 1];
        System.arraycopy(a, j + 1, t1, 0, t1.length);
    
        System.arraycopy(t, 0, a, a.length - l, t.length);
        System.arraycopy(t1, 0, a, i, t1.length);
    }
}