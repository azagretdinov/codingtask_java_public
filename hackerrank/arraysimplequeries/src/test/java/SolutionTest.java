import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class SolutionTest {
    
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();
    
    
    @Test
    public void shouldApplyFirstRule() throws Exception {
        systemInMock.provideLines(
                "8 1",
                "1 2 3 4 5 6 7 8",
                "1 2 4");
        
        String expected = "6\n2 3 4 1 5 6 7 8";
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(expected);
    }
    
    @Test
    public void shouldApplyFirstRuleForOneElement() throws Exception {
        systemInMock.provideLines(
                "8 1",
                "1 2 3 4 5 6 7 1",
                "1 3 3");
        
        String expected = "2\n3 1 2 4 5 6 7 1";
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(expected);
    }
    
    @Test
    public void shouldApplySecondRule() throws Exception {
        systemInMock.provideLines(
                "8 1",
                "1 2 3 4 5 6 7 8",
                "2 3 6");
        
        String expected = "5\n1 2 7 8 3 4 5 6";
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(expected);
    }
    
    @Test
    public void shouldApplySecondRuleForOneElement() throws Exception {
        systemInMock.provideLines(
                "8 1",
                "1 2 3 4 5 6 7 8",
                "2 6 6");
        
        String expected = "5\n1 2 3 4 5 7 8 6";
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(expected);
    }
}