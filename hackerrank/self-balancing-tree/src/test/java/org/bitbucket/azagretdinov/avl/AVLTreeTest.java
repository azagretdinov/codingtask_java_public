package org.bitbucket.azagretdinov.avl;


import org.junit.Test;

import static java.lang.Math.max;
import static org.assertj.core.api.Assertions.assertThat;

public class AVLTreeTest {
    
    @Test
    public void should_insert_root() {
        Node root = AVLTree.insert(null, 5);
    
        assertThat(root.val).isEqualTo(5);
        assertThat(root.ht).isEqualTo(0);
    }
    
    @Test
    public void should_insert_left() {
        Node node = new Node(10);
        
        Node root = AVLTree.insert(node, 5);
        
        assertThat(root.left).isNotNull();
        assertThat(root.left.val).isEqualTo(5);
        
        assertThat(root.ht).isEqualTo(1);
        assertThat(root.left.ht).isEqualTo(0);
    }
    
    @Test
    public void should_insert_right() {
        Node node = new Node(10);
        
        Node root = AVLTree.insert(node, 15);
        
        assertThat(root.right).isNotNull();
        assertThat(root.right.val).isEqualTo(15);
        
        assertThat(root.ht).isEqualTo(1);
        assertThat(root.right.ht).isEqualTo(0);
    }
    
    @Test
    public void should_calculate_height() {
    
        Node root = AVLTree.insert(null, 30);
        root = AVLTree.insert(root, 20);
        root = AVLTree.insert(root, 40);
        root = AVLTree.insert(root, 25);
        root = AVLTree.insert(root, 55);
        root = AVLTree.insert(root, 15);
    
        assertThat(root).isNotNull();
        assertThat(root.ht).isEqualTo(2);
    
        Node node = AVLTree.findNode(root, 20);
        
        assertThat(node).isNotNull();
        assertThat(node.ht).isEqualTo(1);
    
        node = AVLTree.findNode(root, 15);
    
        assertThat(node).isNotNull();
        assertThat(node.ht).isEqualTo(0);
    
    }
    
    @Test
    public void should_rebalance_tree() {
//        insertAndAssertIsBalanced(30, 20, 40, 18, 15);
//        insertAndAssertIsBalanced(30, 20, 40, 18, 22, 23);
//        insertAndAssertIsBalanced(30, 20, 40, 58, 32, 31);
//        insertAndAssertIsBalanced(30, 20, 40, 58, 62, 71);
//        insertAndAssertIsBalanced(3, 2, 4, 5, 6);
        insertAndAssertIsBalanced(14, 25, 21, 10, 23, 7, 26, 12, 30, 16, 19);
    }
    
    @Test
    public void should_return_val() {
        insertAndAssert(12, 14, 25, 21, 10, 23, 7, 26, 12, 30, 16, 19);
    }
    
    private void insertAndAssert(int keyToFind, int...keys) {
        Node root = null;
    
        for (int key : keys) {
            root = AVLTree.insert(root, key);
        }
    
        Node node = AVLTree.findNode(root, keyToFind);
        
        assertThat(node).isNotNull();
    }
    
    private void insertAndAssertIsBalanced(int... keys) {
        Node root = null;
    
        for (int key : keys) {
            root = AVLTree.insert(root, key);
        }
        
        assertThatIsBalanced(root);
    }
    
    private void assertThatIsBalanced(Node root) {
        assertThat(isBalanced(root)).isTrue();
    }
    
    private boolean isBalanced(Node root) {
        if (root == null){
            return true;
        }
        
        boolean b = Math.abs(height(root.left, 1) - height(root.right, 1)) < 2;
        return b && isBalanced(root.left) && isBalanced(root.right);
    }
    
    private int height(Node node, int height){
        if (node == null){
            return height;
        }
        int leftHeight = height(node.left, height + 1);
        int rightHeight = height(node.right, height + 1);
        
        return max(leftHeight, rightHeight);
    }
}