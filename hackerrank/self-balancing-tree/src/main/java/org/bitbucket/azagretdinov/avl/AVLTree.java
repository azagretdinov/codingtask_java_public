package org.bitbucket.azagretdinov.avl;

public class AVLTree {
    
    
    static Node insert(Node node, int val) {
        
        if (node == null) {
            return createNode(val);
        }
        
        if (val < node.val) {
            node.left = insert(node.left, val);
        } else {
            node.right = insert(node.right, val);
        }
        
        node.ht = Math.max(heightOf(node.right), heightOf(node.left)) + 1;
        
        return rebalance(node, val);
    }
    
    private static Node rebalance(final Node z, int val) {
        int heightDelta = heightDelta(z);
        
        
        if (heightDelta > 1 && val < z.left.val) {
            return rightRotate(z);
        }
        
        if (heightDelta > 1 && z.left.val < val) {
            z.left = leftRotate(z.left);
            return rightRotate(z);
        }
    
        if (heightDelta < -1 && z.right.val < val) {
            return leftRotate(z);
        }
    
        if (heightDelta < -1 && val < z.right.val) {
            z.right = rightRotate(z.right);
            return leftRotate(z);
        }
        
        return z;
    }
    
    private static int heightDelta(Node z) {
        return heightOf(z.left) - heightOf(z.right);
    }
    
    private static Node rightRotate(Node node) {
        Node z = node, x = z.left, y = x.right;
        
        x.right = z;
        z.left = y;
    
        x.ht = Math.max(heightOf(x.left), heightOf(x.right)) + 1;
        z.ht = Math.max(heightOf(z.left), heightOf(z.right)) + 1;
        
        return x;
    }
    
    private static Node leftRotate(Node node) {
        Node z = node, x = z.right, y = x.left;
        
        z.right = y;
        x.left = z;
    
        z.ht = Math.max(heightOf(z.left), heightOf(z.right)) + 1;
        x.ht = Math.max(heightOf(x.left), heightOf(x.right)) + 1;
        
        return x;
    }
    
    private static int heightOf(Node node) {
        return node == null ? -1 : node.ht;
    }
    
    private static Node createNode(int val) {
        Node node = new Node();
        node.val = val;
        node.ht = 0;
        return node;
    }
    
    static Node findNode(Node root, int val) {
        
        if (root == null) {
            return null;
        }
        
        Node x = root;
        
        while (x != null) {
            if (x.val == val) {
                return x;
            } else if (val < x.val) {
                x = x.left;
            } else {
                x = x.right;
            }
        }
        
        return null;
    }
}
