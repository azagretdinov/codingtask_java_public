package org.bitbucket.azagretdinov.avl;

public class Node {
    
    int val;   //Value
    int ht;      //Height
    Node left;   //Left child
    Node right;   //Right child
    
    Node(int val) {
        this.val = val;
    }
    
    public Node() {
        
    }
    
    @Override
    public String toString() {
        return "Node{" + "val=" + val +
                            ", ht=" + ht +
                            '}';
    }
}
