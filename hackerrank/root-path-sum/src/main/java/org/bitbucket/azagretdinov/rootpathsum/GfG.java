package org.bitbucket.azagretdinov.rootpathsum;

public class GfG {
    
    /*you are required to complete this function */
    boolean hasPathSum(Node node, int sum)
    {
        if (node == null){
            return false;
        }
        
        if (node.left == null && node.right == null &&  sum == node.data){
            return true;
        }
        
        return hasPathSum(node.left, sum - node.data)
                       || hasPathSum(node.right, sum - node.data);
    }
    
}
