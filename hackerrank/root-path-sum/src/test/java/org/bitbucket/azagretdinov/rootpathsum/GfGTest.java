package org.bitbucket.azagretdinov.rootpathsum;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GfGTest {
    
    private GfG objectUnderTest = new GfG();
    
    @Test
    public void shouldReturnFalseIFTreeIsEmpty() {
        assertThat(objectUnderTest.hasPathSum(null, 10)).isFalse();
        
    }
    
    @Test
    public void shouldReturnFalseIfSumNotEqualsOneNodeTreeValue() {
        Node node = new Node(10);
        assertThat(objectUnderTest.hasPathSum(node, 11)).isFalse();
    }
    
    @Test
    public void shouldReturnTrueIfSumEqualsOneNodeTreeValue() {
        Node node = new Node(10);
        assertThat(objectUnderTest.hasPathSum(node, 10)).isTrue();
    }
    
    @Test
    public void shouldReturnTrueIfSumEqualsSumOfTwoNodesInTwoNodesTree() {
        Node node = new Node(10);
        node.left = new Node(5);
        
        assertThat(objectUnderTest.hasPathSum(node, 15)).isTrue();
    
        node = new Node(10);
        node.right = new Node(5);
    
        assertThat(objectUnderTest.hasPathSum(node, 15)).isTrue();
    }
    
    @Test
    public void shouldReturnFalseIfSumEqualsNodeButNodeIsNotChild() {
        Node node = new Node(10);
        node.left = new Node(5);
        
        assertThat(objectUnderTest.hasPathSum(node, 10)).isFalse();
    
        node = new Node(10);
        node.right = new Node(5);
    
        assertThat(objectUnderTest.hasPathSum(node, 10)).isFalse();
    }
    
}