package org.bitbucket.azagretdinov.twocharacters;


import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TwoCharactersTest {
    
    
    @Test
    public void valid_string_should_non_contain_more_than_2_characters() {
        
        assertThat(TwoCharacters.isValid("xyz")).isFalse();
        assertThat(TwoCharacters.isValid("x")).isFalse();
        assertThat(TwoCharacters.isValid("xy")).isTrue();
        
    }
    
    
    @Test
    public void valid_string_should_not_contain_repetable_chars() {
        assertThat(TwoCharacters.isValid("xyy")).isFalse();
        assertThat(TwoCharacters.isValid("xyxy")).isTrue();
    }
    
    @Test
    public void should_delete_delete_less_freaquanty_char_and_build_a_new_valid() {
    
        assertThat(TwoCharacters.findMaxSubString("beabeefeab")).isEqualTo(5);
    }
    
    
    static class TwoCharacters {
        
        public static boolean isValid(final String s) {
            Set<Character> chars = new HashSet<>();
            
            char previous = 0;
            for (int i = 0; i < s.length(); i++) {
                char charAt = s.charAt(i);
                if (previous == charAt) {
                    return false;
                }
                previous = charAt;
                chars.add(charAt);
            }
            
            return chars.size() == 2;
        }
    
        public static int findMaxSubString(final String s) {
            
            char[][] charMatrix = new char[26][26];
            int[][] counter = new int[26][26];
    
            for (char c: s.toCharArray()) {
                final int index = c - 'a';
    
                for (int j = 0; j < 26; j++) {
                    if(charMatrix[index][j] != c && counter[index][j] != -1) {
                        charMatrix[index][j] = c;
                        ++counter[index][j];
                    } else {
                        counter[index][j] = -1;
                    }
                    if(charMatrix[j][index] != c && counter[j][index] != -1) {
                        charMatrix[j][index] = c;
                        ++counter[j][index];
                    } else {
                        counter[j][index] = -1;
                    }
                }
            }
    
            int max = 0;
            for(int i = 0; i < 26; i++) {
                for(int j = 0; j < 26; j++) {
                    if(counter[i][j] > max) {
                        max = counter[i][j];
                    }
                }
            }
            if(max > 1) {
                return max;
            } else {
                return 0;
            }
            
        }
    }
    
}
