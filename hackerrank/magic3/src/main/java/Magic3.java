import java.math.BigDecimal;
import java.util.Scanner;

public class Magic3 {

    public static void main(String[] args){
        System.out.print("Please, enter number:");
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();
        System.out.println(String.format("Multiple: %s", findMultiple(number)));
    }

    private static void assertThatEquals(long out, int expected) {
        if (out != expected){
            throw new AssertionError(String.format("Expected: %s, but actually %s", expected, out));
        }
    }

    private static BigDecimal findMultiple(int number) {
        int count =1;
        int remaining = 1;

        while (remaining != 0){
            remaining = (remaining * 10 + 1) % number;
            count++;
        }

        char[] result = new char[count];
        while (count > 0){
            result[--count] = '1';
        }
        return new BigDecimal(new String(result));
    }

}
