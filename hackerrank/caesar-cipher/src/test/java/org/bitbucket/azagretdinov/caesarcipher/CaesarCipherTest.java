package org.bitbucket.azagretdinov.caesarcipher;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CaesarCipherTest {
    
    @Test
    public void should_encrypt_lower_case_chars() {
        assertThat(CaesarCipher.encrypt("mi", 2)).isEqualTo("ok");
        assertThat(CaesarCipher.encrypt("miz", 2)).isEqualTo("okb");
        assertThat(CaesarCipher.encrypt("www.abc.xy", 87)).isEqualTo("fff.jkl.gh");
    }
    
    @Test
    public void should_encrypt_uppercase_case_chars() {
        assertThat(CaesarCipher.encrypt("O", 2)).isEqualTo("Q");
        assertThat(CaesarCipher.encrypt("OZ", 2)).isEqualTo("QB");
    }
    
    @Test
    public void should_encrypt_only_letters() {
        assertThat(CaesarCipher.encrypt("middle-Outz", 2)).isEqualTo("okffng-Qwvb");
        assertThat(Integer.MAX_VALUE).isGreaterThan((int) Math.pow(10, 9));
    }
    
    private static class CaesarCipher {
        public static String encrypt(final String in, final int key) {
            char[] encrypted = new char[in.length()];
            
            for (int i = 0; i < in.length(); i++) {
                char originalChar = in.charAt(i);
                char encryptedChar;
                if (Character.isLetter(originalChar)) {
                    encryptedChar = (char) (originalChar + (char) key);
                    
                    char upperBound = Character.isUpperCase(originalChar) ? 'Z' : 'z';
                    char lowerBound = Character.isUpperCase(originalChar) ? 'A' : 'a';
                    
                    while (encryptedChar > upperBound) {
                        encryptedChar = (char) (lowerBound + (encryptedChar - upperBound) - 1);
                    }
                } else {
                    encryptedChar = originalChar;
                }
                encrypted[i] = encryptedChar;
            }
            
            return new String(encrypted);
        }
    }
}
