package org.bitbucket.azagretdinov;

class Solution {
    public int solution(int[] a) {
        
        if (a.length < 0) {
            return -1;
        }
        
        int s = sum(a),
                p = 0,
                sLeft = 0;
        
        while ((p + 1) < a.length && s - a[p] != sLeft) {
            sLeft += a[p];
            p++;
            s -= sLeft;
        }
        
        return p == a.length ? -1 : p;
    }
    
    private int sum(int[] a) {
        int s = 0;
        for (int anA : a) {
            s += anA;
        }
        return s;
    }
}
