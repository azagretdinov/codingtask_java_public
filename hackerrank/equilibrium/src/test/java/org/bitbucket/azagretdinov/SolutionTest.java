package org.bitbucket.azagretdinov;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SolutionTest {
    
    private Solution solution;
    
    @Before
    public void setUp() throws Exception {
        solution = new Solution();
    }
    
    @Test
    public void shouldReturnZero() {
        int[] A = new int[8];
        A[0] = -1;
        A[1] = 3;
        A[2] = -4;
        A[3] = 5;
        A[4] = 1;
        A[5] = -6;
        A[6] = 2;
        A[7] = 1;
        
        int p = solution.solution(A);
        
        assertThat(p).isEqualTo(1);
    }
    
    @Test
    public void extremeNegativeNumbers() {
        int[] A = new int[]{-2147483648};
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(0);
    }
    
    @Test
    public void shouldReturnValueIfSumZero() {
        int[] A = new int[]{1, 2, -3, 0};
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(3);
    }
    
    @Test
    public void singleEmpty() {
    
        int[] A = new int[]{0};
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(0);
    }
    
    @Test
    public void combinationsOfTwo() {
        int[] A = new int[]{-1, 0};
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(0);
    }
    
    @Test
    public void combinationsOfThree() {
        int[] A = new int[]{1, -1, -1};
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(2);
    }
    
    @Test
    public void longSeqOfOnes() {
        int n = 49998;
        int[] A = new int[n*2 + 1];
        for (int i = 0; i < A.length; i++){
            A[i] = 1;
        }
    
        int p = solution.solution(A);
    
        assertThat(p).isEqualTo(99996);
    }
}