package org.bitbucket.azagretdinov.algorithms.clonegraph;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SolutionTest {
    
    @Test
    public void shouldCopyTreeGraphWithoutLoop() {
    
        UndirectedGraphNode node = new UndirectedGraphNode(0);
        UndirectedGraphNode node1 = new UndirectedGraphNode(1);
        UndirectedGraphNode node2 = new UndirectedGraphNode(2);
        
        node.neighbors.add(node1);
        node.neighbors.add(node2);
    
        Solution solution = new Solution();
        UndirectedGraphNode copy = solution.cloneGraph(node);
    
        assertThat(copy).isNotNull();
        assertThat(copy.label).isEqualTo(0);
        assertThat(copy.neighbors).extracting("label").containsExactly(1, 2);
        
    }
    
    @Test
    public void shouldCopyGraphWithLoop() {
        UndirectedGraphNode node = new UndirectedGraphNode(0);
        node.neighbors.add(node);
        node.neighbors.add(node);
        
        Solution solution = new Solution();
        UndirectedGraphNode copy = solution.cloneGraph(node);
    
        assertThat(copy).isNotNull();
        assertThat(copy.neighbors).extracting("label").containsExactly(0, 0);
    }
}