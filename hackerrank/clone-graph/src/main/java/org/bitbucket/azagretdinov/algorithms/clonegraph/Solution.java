package org.bitbucket.azagretdinov.algorithms.clonegraph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class Solution {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null) {
            return null;
        }
        
        if (node.neighbors.isEmpty()) {
            return new UndirectedGraphNode(node.label);
        }
        
        Queue<UndirectedGraphNode> queue = new LinkedList<>();
        Map<UndirectedGraphNode, Color> colors = new HashMap<>();
        Map<UndirectedGraphNode, UndirectedGraphNode> copies = new HashMap<>();
        
        UndirectedGraphNode root = new UndirectedGraphNode(node.label);
        
        colors.put(node, Color.Gray);
        copies.put(node, root);
        
        queue.add(node);
        
        
        while (!queue.isEmpty()) {
            UndirectedGraphNode source = queue.poll();
            
            if (colors.get(source) != Color.Black) {
                
                UndirectedGraphNode copyOfSource = copies.get(source);
    
                for (UndirectedGraphNode neighbor : source.neighbors) {
                    UndirectedGraphNode copy;
                    if (colors.getOrDefault(neighbor, Color.White) == Color.White) {
                        copy = new UndirectedGraphNode(neighbor.label);
                        copies.put(neighbor, copy);
    
                        colors.put(neighbor, Color.Gray);
                        queue.add(neighbor);
                    }else {
                        copy = copies.get(neighbor);
                    }
                    copyOfSource.neighbors.add(copy);
                }
    
                colors.put(source, Color.Black);
            }
        }
        
        return root;
    }
    
    enum Color {
        White, Gray, Black
    }
}
