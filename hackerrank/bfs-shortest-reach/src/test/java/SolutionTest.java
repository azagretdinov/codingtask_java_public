import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class SolutionTest {
    
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();
    
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();
    
    @Test
    public void shouldFindShortPath() throws IOException {
        systemInMock.provideLines(
                "1",
                "4 2",
                "1 2",
                "1 3",
                "1  "
        );
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(
                "6 6 -1"
        );
    }
    
    @Test
    public void shouldFindShortPath2() throws IOException {
        systemInMock.provideLines(
                "1",
                "3 1",
                "2 3",
                "2  "
        );
        
        Solution.main(new String[0]);
        
        assertThat(systemOutRule.getLog()).isEqualTo(
                "-1 6"
        );
    }
    
}