import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Queue;

public class Solution {
    
    private static final int MULTIPLIER = 6;
    
    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int q = Integer.parseInt(in.readLine());
        
        for (int i = 0; i < q; i++) {
            Graph g = new GraphReader(in).read();
            int s = Integer.parseInt(in.readLine().trim()) - 1;
            g.bfs(s);
            new GraphWriter(g).print(System.out);
        }
        
    }
    
    public enum Color {
        WHITE, GRAY, BLACK
    }
    
    public static class GraphReader {
        
        private BufferedReader in;
        
        private GraphReader(BufferedReader in) {
            this.in = in;
        }
        
        private Graph read() throws IOException {
            String[] input = in.readLine().split(" ");
            int n = Integer.parseInt(input[0]);
            int m = Integer.parseInt(input[1]);
            
            Graph g = new Graph(n);
            
            for (int i = 0; i < m; i++) {
                input = in.readLine().split(" ");
                int u = Integer.parseInt(input[0]) - 1;
                int v = Integer.parseInt(input[1]) - 1;
                
                g.addEdge(u, v);
            }
            
            return g;
        }
    }
    
    public static class Graph {
        private Edge[] edges;
        private Attributes[] attributes;
        
        private Graph(int n) {
            edges = new Edge[n];
            attributes = new Attributes[n];
            for (int i = 0; i < n; i++) {
                addVertex(i);
            }
        }
        
        private void addVertex(int u) {
            if (attributes[u] == null) {
                Attributes a = new Attributes();
                a.color = Color.WHITE;
                a.d = -1;
                attributes[u] = a;
            }
        }
        
        private void addEdge(int u, int v) {
            addVertex(u);
            addVertex(v);
            
            addEdge(u, v, edges[u]);
            addEdge(v, u, edges[v]);
        }
        
        private void addEdge(int vertOut, int vertIn, Edge edge) {
            Edge newEdge = new Edge();
            newEdge.vertex = vertIn;
            
            if (edge != null) {
                newEdge.next = edge;
            }
            edges[vertOut] = newEdge;
        }
        
        public void bfs(int s) {
            Queue<Integer> q = new LinkedList<>();
            
            attributes[s].color = Color.GRAY;
            attributes[s].d = 0;
            
            q.add(s);
            while (!q.isEmpty()) {
                int u = q.poll();
                
                Attributes au = attributes[u];
                
                Edge edge = edges[u];
                while (edge != null) {
                    Attributes a = attributes[edge.vertex];
                    if (a.color == Color.WHITE) {
                        
                        a.color = Color.GRAY;
                        a.d = au.d + 1;
                        
                        q.add(edge.vertex);
                    }
                    edge = edge.next;
                }
                au.color = Color.BLACK;
            }
        }
    }
    
    public static class Edge {
        private int vertex;
        private Edge next;
    }
    
    public static class Attributes {
        private Color color;
        private int d;
    }
    
    public static class GraphWriter {
        private final Graph graph;
    
        public GraphWriter(Graph graph) {
            this.graph = graph;
        }
        
        public void print(PrintStream out) {
            String s = "";
            for (Attributes attribute : graph.attributes) {
                if (attribute.d < 0){
                    s += attribute.d;
                    s += " ";
                } else if (attribute.d > 0){
                    s += attribute.d * MULTIPLIER;
                    s += " ";
                }
            }
            out.println(s.trim());
        }
    }
}