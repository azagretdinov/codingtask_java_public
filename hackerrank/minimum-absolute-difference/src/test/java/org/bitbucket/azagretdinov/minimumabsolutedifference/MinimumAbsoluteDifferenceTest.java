package org.bitbucket.azagretdinov.minimumabsolutedifference;

import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class MinimumAbsoluteDifferenceTest {
    
    @Test
    public void should_return_minimum_absolute_difference_when_positive_and_negative_in_array() {
        
        assertThat(minimumAbsoluteDifference(new int[]{3, -7, 0})).isEqualTo(3);
        assertThat(minimumAbsoluteDifference(new int[]{3, -7, 3})).isEqualTo(0);
        assertThat(minimumAbsoluteDifference(new int[]{3, -7, 4})).isEqualTo(1);
        assertThat(minimumAbsoluteDifference(new int[]{3, -7, 15})).isEqualTo(10);
        
    }
    
    @Test
    public void should_return_minimum_absolute_difference_when_only_positive__in_array() {
        
        assertThat(minimumAbsoluteDifference(new int[]{3, 3})).isEqualTo(0);
        assertThat(minimumAbsoluteDifference(new int[]{3, 4})).isEqualTo(1);
        assertThat(minimumAbsoluteDifference(new int[]{3, 7, 15})).isEqualTo(4);
        assertThat(minimumAbsoluteDifference(new int[]{3, 0, 5})).isEqualTo(2);
        
    }
    
    
    public static long minimumAbsoluteDifference(int[] a) {
        final int length = a.length;
        Arrays.sort(a);
        
        int min = Integer.MAX_VALUE;
//        int  = 0;
//
//        for (int i = 0; i < length; i++) {
//            final int abs = Math.abs(a[i]);
//            if (abs < min){
//                min = abs;
//            }
//            if (max < abs){
//                max = abs;
//            }
//        }
        
        return min;
    }
}
