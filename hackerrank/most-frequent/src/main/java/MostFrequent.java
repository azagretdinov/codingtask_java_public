import java.util.Arrays;
import java.util.Objects;

public class MostFrequent {

    public static void main(String[] args) {
        Integer[] a = getArray(1, 3, 4, 5, 2, 2, 3, 2, 3, 3);

        assertThat(findMostFrequent(a), new int[]{3, 4});

    }

    private static void assertThat(int[] actual, int[] expected) {
        if (actual.length != expected.length) {
            throwException(actual, expected);
        }
        for (int i = 0; i < expected.length; i++) {
            if (expected[i] != actual[i]) {
                throwException(actual, expected);
            }
        }

    }

    private static void throwException(int[] actual, int[] expected) {
        throw new AssertionError(String.format("Expected %s, but actually %s.", Arrays.toString(expected), Arrays.toString(actual)));
    }

    private static int[] findMostFrequent(Integer[] a) {
        Arrays.sort(a, (o1, o2) -> o2 - o1);
        int maxFrequent = a[0];
        int maxFrequentCount = 1;
        int frequentCount = 1;
        for (int i = 1; i < a.length; i++) {
            if (Objects.equals(a[i - 1], a[i])) {
                frequentCount++;
                if (frequentCount >= maxFrequentCount) {
                    maxFrequentCount = frequentCount;
                    maxFrequent = a[i];
                }
            }else{
                frequentCount = 1;
            }
        }
        return new int[]{maxFrequent, maxFrequentCount};
    }

    private static Integer[] getArray(Integer... a) {
        return a;
    }

}
