package org.bitbucket.azagretdinov.algorithms.btheigth;

/**
 *
 */
public class Solution {
    
    static int height(Node root) {
        if(root == null){
            return 0;
        }
        
        int leftHeight = 0;
        if(root.left != null){
            leftHeight = height(root.left) + 1;
        }
        
        int rightHeight = 0;
        if(root.right != null){
            rightHeight = height(root.right) + 1;
        }
        
        return rightHeight > leftHeight ? rightHeight: leftHeight;
    }
}
