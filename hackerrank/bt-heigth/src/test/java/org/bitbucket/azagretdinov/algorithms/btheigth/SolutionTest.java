package org.bitbucket.azagretdinov.algorithms.btheigth;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SolutionTest {
    
    @Test
    public void shouldReturn3() {
        
        Node root = new Node(3);
        root.left = new Node(2);
        root.left = new Node(1);
        root.right = new Node(5);
        root.right.left = new Node(4);
        root.right.right = new Node(6);
        root.right.right.right = new Node(7);
        
        int height = Solution.height(root);
        
        assertThat(height).isEqualTo(3);
        
    }
    
}