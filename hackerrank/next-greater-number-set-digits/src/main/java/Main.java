import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int number = in.nextInt();

        String[] r = new String[number];
        for (int i = 0; i < number; i++) {
            r[i] = nextGreaterNumberSetDigits(in.next());
        }
        for (String a : r) {
            System.out.println(a);
        }
    }

    private static String nextGreaterNumberSetDigits(String in) {
        try {
            int[] digits = toDigits(in);
            return toString(findMinPossible(digits));
        } catch (IllegalArgumentException e) {
            return "not possible";
        }
    }

    private static String toString(int[] digits) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int digit : digits) {
            stringBuilder.append(digit);
        }
        return stringBuilder.toString();
    }

    private static int[] findMinPossible(int[] digits) {
        int i = digits.length - 1;
        while (0 < i && digits[i] <= digits[i - 1]) {
            i--;
        }
        if (i == 0) {
            throw new IllegalArgumentException();
        }

        int x = digits[i - 1];
        int minIndex = i;
        for (int j = i + 1; j < digits.length; j++) {
            if (digits[j] > x && digits[j] < digits[minIndex]) {
                minIndex = j;
            }
        }

        swap(digits, i - 1, minIndex);

        Arrays.sort(digits, i, digits.length);

        return digits;
    }

    private static void swap(int[] digits, int from, int to) {
        int t = digits[from];
        digits[from] = digits[to];
        digits[to] = t;
    }

    private static int[] toDigits(String s) {
        int[] r = new int[s.length()];
        for (int i = 0; i < s.length(); i++) {
            r[i] = s.charAt(i) - 48;
        }
        return r;
    }
}
